package com.bank.credits.common.exception;

public class BankCreditsException extends RuntimeException {

	public BankCreditsException() {

	}

	public BankCreditsException(String message) {

		super(message);
	}

	public BankCreditsException(Throwable cause) {

		super(cause);
	}

	public BankCreditsException(String message, Throwable cause) {

		super(message, cause);
	}
}
