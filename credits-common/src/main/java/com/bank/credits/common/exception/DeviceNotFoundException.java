package com.bank.credits.common.exception;

public class DeviceNotFoundException extends BankCreditsException{

	public DeviceNotFoundException() {

		super();
	}

	public DeviceNotFoundException(String message) {

		super(message);
	}

	public DeviceNotFoundException(String message, Throwable cause) {

		super(message, cause);
	}

	public DeviceNotFoundException(Throwable cause) {

		super(cause);
	}
}
