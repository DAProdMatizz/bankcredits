package com.bank.credits.common.exception;

public class DtoException extends BankCreditsException {

	public DtoException(String message) {

		super(message);
	}

	public DtoException(Throwable cause) {

		super(cause);
	}
}
