package com.bank.credits.common.exception;

public class JsonGenerationException extends DtoException {

	public JsonGenerationException(String message) {

		super(message);
	}

	public JsonGenerationException(Throwable cause) {

		super(cause);
	}
}
