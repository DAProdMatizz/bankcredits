package com.bank.credits.common.exception;

public class NonInstanceException extends BankCreditsException {

	public NonInstanceException(Class<?> clazz) {

		super("There is no instances for " + clazz.getName() + " class");
	}
}
