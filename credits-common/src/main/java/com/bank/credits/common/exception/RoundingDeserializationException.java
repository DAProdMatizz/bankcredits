package com.bank.credits.common.exception;

public class RoundingDeserializationException extends BankCreditsException {

	public RoundingDeserializationException(Throwable e, String message) {

		super(e.getMessage() + ": " + message);
	}

	public RoundingDeserializationException(String message) {

		super(message);
	}
}
