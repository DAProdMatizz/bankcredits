package com.bank.credits.common.exception;

public class RoundingNumberException extends DtoException {

	public RoundingNumberException(Number number) {

		super("The number " + number + " of class " + number.getClass() + " can't be rounded.");
	}
}
