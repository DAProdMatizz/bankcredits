package com.bank.credits.common.utils;

import com.bank.credits.common.exception.NonInstanceException;

import java.util.Collection;
import java.util.Objects;

public final class CollectionUtils {

	private CollectionUtils() {

		throw new NonInstanceException(CollectionUtils.class);
	}

	public static <T> boolean notContains(Collection<T> collection, T value) {

		return !collection.contains(value);
	}

	public static <T> boolean contains(Collection<T> collection, T value) {

		return collection.contains(value);
	}

	public static boolean nullOrEmpty(Collection<?> collection) {

		return Objects.isNull(collection) || collection.isEmpty();
	}

	public static boolean nonNullNonEmpty(Collection<?> collection) {

		return Objects.nonNull(collection) && !collection.isEmpty();
	}
}
