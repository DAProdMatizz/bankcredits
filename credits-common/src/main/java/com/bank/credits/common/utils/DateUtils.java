package com.bank.credits.common.utils;

import com.bank.credits.common.exception.NonInstanceException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

public final class DateUtils {

	public static final String APPLICATION_DATE_PATTERN = "yyyy/MM/dd";
	public static final DateFormat APPLICATION_DATE_FORMAT = new SimpleDateFormat(APPLICATION_DATE_PATTERN);

	private DateUtils() {

		throw new NonInstanceException(DateUtils.class);
	}

	public static Date addMilliseconds(Date date, long ms) {

		return org.apache.commons.lang3.time.DateUtils.addMilliseconds(date, (int) ms);
	}

	public static Date addSeconds(Date date, int seconds) {

		return org.apache.commons.lang3.time.DateUtils.addSeconds(date, seconds);
	}

	public static Date addHours(Date date, int hours) {

		return org.apache.commons.lang3.time.DateUtils.addHours(date, hours);
	}

	public static Date addMinutes(Date date, int minutes) {

		return org.apache.commons.lang3.time.DateUtils.addMinutes(date, minutes);
	}

	public static Date addDays(Date date, int days) {

		return org.apache.commons.lang3.time.DateUtils.addDays(date, days);
	}

	public static Date now() {

		return new Date();
	}

	public static Date from(int year, int month, int day) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		return calendar.getTime();
	}

	public static LocalDate toLocalDate(Date date) {

		return date.toInstant().atZone(ZoneOffset.UTC.normalized()).toLocalDate();
	}
}
