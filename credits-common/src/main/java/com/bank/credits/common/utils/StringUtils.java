package com.bank.credits.common.utils;

import com.bank.credits.common.exception.NonInstanceException;

import java.util.Objects;

public final class StringUtils {

	public static final String SPACE = " ";
	public static final String EMPTY = "";
	public static final String RIGHT_SLASH = "/";
	public static final String PERCENT = "%";
	public static final String DOT = ".";
	public static final String COMMA = ",";
	public static final String RIGHT_BRACKET = "]";
	public static final String LEFT_BRACKET = "[";
	public static final String RIGHT_ANGLE_BRACKET = ">";
	public static final String LEFT_ANGLE_BRACKET = "<";
	public static final String HYPHEN = "-";
	public static final String VERTICAL_BAR = "|";
	public static final String COMMA_SPACE = COMMA + SPACE;
	public static final String CSV = "csv";
	public static final String PNG = "png";
	public static final String UNDERSCORE = "_";
	public static final String QUOTE = "'";
	public static final String ZERO_REPLACEMENT_SYMBOL = "{0}";
	public static final String FIRST_REPLACEMENT_SYMBOL = "{1}";
	public static final String PLUS = "+";
	public static final String DOUBLE_DOT = ":";
	public static final String QUOTE_MARK = "\"";
	public static final String EQUALS_MARK = "=";
	public static final String COLON = ":";
	public static final String NEW_LINE = "\n";

	private StringUtils() {

		throw new NonInstanceException(StringUtils.class);
	}

	public static boolean nullOrEmpty(String str) {

		return !nonNullNonEmpty(str, false);
	}

	public static boolean nonNullNonEmpty(String str) {

		return nonNullNonEmpty(str, false);
	}

	public static boolean nonNullNonEmpty(String value, boolean trim) {

		if (Objects.isNull(value)) {
			return false;
		}

		return trim && !value.trim().isEmpty() || !trim && !value.isEmpty();
	}

	public static boolean isNumeric(String str) {

		return org.apache.commons.lang3.StringUtils.isNumeric(str);
	}
}

