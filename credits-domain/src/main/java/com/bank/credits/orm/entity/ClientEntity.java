package com.bank.credits.orm.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clients")
public class ClientEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String firstName;

	private String secondName;

	private String email;

	public ClientEntity() {

	}

	public ClientEntity(String firstName, String secondName, String email) {

		this.firstName = firstName;
		this.secondName = secondName;
		this.email = email;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getFirstName() {

		return firstName;
	}

	public void setFirstName(String forename) {

		this.firstName = forename;
	}

	public String getSecondName() {

		return secondName;
	}

	public void setSecondName(String surname) {

		this.secondName = surname;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}
}
