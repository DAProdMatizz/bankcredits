package com.bank.credits.orm.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "credits")
public class CreditEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "client_id")
	private ClientEntity client;

	@ManyToOne
	@JoinColumn(name = "credit_type_id")
	private CreditTypeEntity creditType;

	@ManyToOne
	@JoinColumn(name = "creditor_id")
	private CreditorEntity creditor;

	private BigDecimal number;

	@Column(name = "repayment")
	private boolean payedOff;

	private Date openingDate;

	private BigDecimal sum;

	private BigDecimal monthsAmount;

	public CreditEntity() {

	}

	public CreditEntity(ClientEntity client, CreditTypeEntity creditType, CreditorEntity creditor, BigDecimal number, Date openingDate, BigDecimal sum, BigDecimal monthsAmount) {

		this.client = client;
		this.creditType = creditType;
		this.creditor = creditor;
		this.number = number;
		this.openingDate = openingDate;
		this.sum = sum;
		this.monthsAmount = monthsAmount;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public ClientEntity getClient() {

		return client;
	}

	public void setClient(ClientEntity client) {

		this.client = client;
	}

	public CreditTypeEntity getCreditType() {

		return creditType;
	}

	public void setCreditType(CreditTypeEntity creditType) {

		this.creditType = creditType;
	}

	public CreditorEntity getCreditor() {

		return creditor;
	}

	public void setCreditor(CreditorEntity creditor) {

		this.creditor = creditor;
	}

	public BigDecimal getNumber() {

		return number;
	}

	public void setNumber(BigDecimal num) {

		this.number = num;
	}

	public boolean isPayedOff() {

		return payedOff;
	}

	public void setPayedOff(boolean payedOff) {

		this.payedOff = payedOff;
	}

	public Date getOpeningDate() {

		return openingDate;
	}

	public void setOpeningDate(Date time) {

		this.openingDate = time;
	}

	public BigDecimal getSum() {

		return sum;
	}

	public void setSum(BigDecimal sum) {

		this.sum = sum;
	}

	public BigDecimal getMonthsAmount() {

		return monthsAmount;
	}

	public void setMonthsAmount(BigDecimal monthsAmount) {

		this.monthsAmount = monthsAmount;
	}
}
