package com.bank.credits.orm.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "credit_types")
public class CreditTypeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private BigDecimal rate;

	private Integer minSum;

	private Integer minMonths;

	public CreditTypeEntity() {

	}

	public CreditTypeEntity(String name, BigDecimal rate, Integer minSum, Integer minMonths) {

		this.name = name;
		this.rate = rate;
		this.minSum = minSum;
		this.minMonths = minMonths;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public BigDecimal getRate() {

		return rate;
	}

	public void setRate(BigDecimal rate) {

		this.rate = rate;
	}

	public Integer getMinSum() {

		return minSum;
	}

	public void setMinSum(Integer minSum) {

		this.minSum = minSum;
	}

	public Integer getMinMonths() {

		return minMonths;
	}

	public void setMinMonths(Integer minMonths) {

		this.minMonths = minMonths;
	}
}
