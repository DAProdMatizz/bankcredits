package com.bank.credits.orm.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "repayments")
public class RepaymentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "credit_id")
	private CreditEntity credit;

	private Date repaymentDate;

	private BigDecimal sum;

	public RepaymentEntity() {

	}

	public RepaymentEntity(CreditEntity credit, Date repaymentDate, BigDecimal sum) {

		this.credit = credit;
		this.repaymentDate = repaymentDate;
		this.sum = sum;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public CreditEntity getCredit() {

		return credit;
	}

	public void setCredit(CreditEntity credit) {

		this.credit = credit;
	}

	public Date getRepaymentDate() {

		return repaymentDate;
	}

	public void setRepaymentDate(Date time) {

		this.repaymentDate = time;
	}

	public BigDecimal getSum() {

		return sum;
	}

	public void setSum(BigDecimal sum) {

		this.sum = sum;
	}
}
