package com.bank.credits.orm.repository;

import com.bank.credits.orm.entity.CreditEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CreditRepository extends JpaRepository<CreditEntity, Long> {

	@Query("select e from CreditEntity e where e.client.id = :clientId")
	List<CreditEntity> findAllByClientId(@Param("clientId") Long clientId);
}
