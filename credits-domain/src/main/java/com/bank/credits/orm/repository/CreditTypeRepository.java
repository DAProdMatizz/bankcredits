package com.bank.credits.orm.repository;

import com.bank.credits.orm.entity.CreditTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditTypeRepository extends JpaRepository<CreditTypeEntity, Long> {

}
