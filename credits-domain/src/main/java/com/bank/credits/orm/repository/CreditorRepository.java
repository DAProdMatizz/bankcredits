package com.bank.credits.orm.repository;

import com.bank.credits.orm.entity.CreditorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditorRepository extends JpaRepository<CreditorEntity, Long> {

}
