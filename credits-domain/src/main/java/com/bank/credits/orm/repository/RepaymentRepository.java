package com.bank.credits.orm.repository;

import com.bank.credits.orm.entity.RepaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RepaymentRepository extends JpaRepository<RepaymentEntity, Long> {

	@Query("select e from RepaymentEntity e where e.credit.id = :creditId")
	List<RepaymentEntity> findAllByCreditId(@Param("creditId") Long creditId);
}
