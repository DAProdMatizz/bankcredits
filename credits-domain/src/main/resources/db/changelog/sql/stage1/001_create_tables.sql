begin;

create table clients
(
    id          bigserial primary key,
    first_name  varchar(63)        not null,
    second_name varchar(63)        not null,
    email       varchar(63) unique not null
);

create table creditors
(
    id   bigserial primary key,
    name varchar(63) unique not null
);

create table credit_types
(
    id   bigserial primary key,
    name varchar(63) unique not null,
    rate real               not null
);

create table credits
(
    id             bigserial primary key,
    client_id      integer not null
        constraint credit_client_id references clients (id),
    credit_type_id integer not null
        constraint credit_credit_type_id references credit_types (id),
    creditor_id    integer not null
        constraint credit_creditor_id references creditors (id),
    number         integer not null,
    repayment      boolean,
    opening_date   date
);

create table repayments
(
    id             bigserial primary key,
    credit_id      integer not null
        constraint repayment_credit_id references credits (id),
    repayment_date date,
    sum            real    not null
);

commit;
