begin;

alter table credits alter column repayment set not null;
alter table credits alter column repayment set default false;
alter table credits alter column opening_date set not null;
alter table credits alter column opening_date set default now();
alter table repayments alter column repayment_date set not null;
alter table repayments alter column repayment_date set default now();

commit;
