begin;

create temporary table mytable_credits as
select credits.*, 12 as months_amount from credits;

create temporary table mytable_repayments as
select * from repayments;

delete from repayments where id > 0;

delete from credits where id > 0;

alter table credits
	add months_amount integer not null;

insert into credits select * from mytable_credits;

insert into repayments select * from mytable_repayments;

commit;
