begin;

alter table credit_types
    add min_sum int default 0 not null;

alter table credit_types
    add min_months int default 0 not null;

commit;
