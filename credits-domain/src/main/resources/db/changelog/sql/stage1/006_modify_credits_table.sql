begin;

alter table credits alter column number type bigint using number::bigint;

alter table credits alter column sum type bigint using sum::bigint;

alter table credits alter column months_amount type bigint using months_amount::bigint;

commit;
