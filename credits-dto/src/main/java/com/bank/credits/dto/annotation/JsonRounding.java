package com.bank.credits.dto.annotation;

import com.bank.credits.dto.processor.RoundingDeserializer;
import com.bank.credits.dto.processor.RoundingSerializer;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = RoundingSerializer.class)
@JsonDeserialize(using = RoundingDeserializer.class)
public @interface JsonRounding {

	int precision() default 2;

	boolean useBigDecimalRoundingDeserializer() default true;
}
