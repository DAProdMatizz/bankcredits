package com.bank.credits.dto.authenticate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HandshakeDto {

	private final String token;
	private final String username;
	private final String role;
	private final String identity;

	@JsonCreator
	public HandshakeDto(@JsonProperty("token") String token, @JsonProperty("username") String username, @JsonProperty("role") String role,
			@JsonProperty("identity") String identity) {

		this.token = token;
		this.username = username;
		this.role = role;
		this.identity = identity;
	}

	public String getToken() {

		return token;
	}

	public String getUsername() {

		return username;
	}

	public String getRole() {

		return role;
	}

	public String getIdentity() {

		return identity;
	}
}
