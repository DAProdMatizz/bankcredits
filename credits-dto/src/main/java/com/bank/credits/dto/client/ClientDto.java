package com.bank.credits.dto.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientDto {

	private final Long id;
	private final String firstName;
	private final String secondName;
	private final String email;

	@JsonCreator
	public ClientDto(@JsonProperty("id") Long id, @JsonProperty("firstName") String firstName, @JsonProperty("secondName") String secondName, @JsonProperty("email") String email) {

		this.id = id;
		this.firstName = firstName;
		this.secondName = secondName;
		this.email = email;
	}

	public Long getId() {

		return id;
	}

	public String getFirstName() {

		return firstName;
	}

	public String getSecondName() {

		return secondName;
	}

	public String getEmail() {

		return email;
	}
}
