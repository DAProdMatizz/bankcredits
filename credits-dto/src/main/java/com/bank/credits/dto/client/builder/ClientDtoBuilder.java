package com.bank.credits.dto.client.builder;

import com.bank.credits.dto.client.ClientDto;

public class ClientDtoBuilder {

	private Long id;
	private String firstName;
	private String secondName;
	private String email;

	private ClientDtoBuilder() {

	}

	public static ClientDtoBuilder newBuilder() {

		return new ClientDtoBuilder();
	}

	public ClientDtoBuilder id(Long id) {

		this.id = id;
		return this;
	}

	public ClientDtoBuilder firstName(String firstName) {

		this.firstName = firstName;
		return this;
	}

	public ClientDtoBuilder secondName(String secondName) {

		this.secondName = secondName;
		return this;
	}

	public ClientDtoBuilder email(String email) {

		this.email = email;
		return this;
	}

	public ClientDto build() {

		return new ClientDto(id, firstName, secondName, email);
	}
}
