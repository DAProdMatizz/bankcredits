package com.bank.credits.dto.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StringWrapper {

	private final String value;

	@JsonCreator
	public StringWrapper(@JsonProperty("value") String value) {

		this.value = value;
	}

	public String getValue() {

		return value;
	}
}
