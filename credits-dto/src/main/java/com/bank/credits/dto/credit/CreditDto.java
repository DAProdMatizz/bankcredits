package com.bank.credits.dto.credit;

import com.bank.credits.common.utils.DateUtils;
import com.bank.credits.dto.client.ClientDto;
import com.bank.credits.dto.creditor.CreditorDto;
import com.bank.credits.dto.repayment.RepaymentDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class CreditDto {

	private final Long id;
	private final ClientDto client;
	private final CreditTypeDto creditType;
	private final CreditorDto creditor;
	private final BigDecimal creditNumber;
	private final boolean payedOff;
	private final Date openingDate;
	private final BigDecimal fullSum;
	private final BigDecimal monthsAmount;
	private final List<RepaymentDto> repayments;

	@JsonCreator
	public CreditDto(@JsonProperty("id") Long id, @JsonProperty("client") ClientDto client, @JsonProperty("creditType") CreditTypeDto creditType,
			@JsonProperty("creditor") CreditorDto creditor, @JsonProperty("creditNumber") BigDecimal creditNumber, @JsonProperty("payedOff") boolean payedOff,
			@JsonFormat(pattern = DateUtils.APPLICATION_DATE_PATTERN) @JsonProperty("openingDate") Date openingDate, @JsonProperty("fullSum") BigDecimal fullSum,
			@JsonProperty("monthsAmount") BigDecimal monthsAmount, @JsonProperty("repayments") List<RepaymentDto> repayments) {

		this.id = id;
		this.client = client;
		this.creditType = creditType;
		this.creditor = creditor;
		this.creditNumber = creditNumber;
		this.payedOff = payedOff;
		this.openingDate = openingDate;
		this.fullSum = fullSum;
		this.monthsAmount = monthsAmount;
		this.repayments = repayments;
	}

	public Long getId() {

		return id;
	}

	public ClientDto getClient() {

		return client;
	}

	public CreditTypeDto getCreditType() {

		return creditType;
	}

	public CreditorDto getCreditor() {

		return creditor;
	}

	public BigDecimal getCreditNumber() {

		return creditNumber;
	}

	public boolean isPayedOff() {

		return payedOff;
	}

	public Date getOpeningDate() {

		return openingDate;
	}

	public BigDecimal getFullSum() {

		return fullSum;
	}

	public BigDecimal getMonthsAmount() {

		return monthsAmount;
	}

	public List<RepaymentDto> getRepayments() {

		return repayments;
	}
}
