package com.bank.credits.dto.credit;

import com.bank.credits.dto.client.ClientDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CreditRequestDto {

	private final ClientDto client;
	private final Long creditTypeId;
	private final Long creditorId;
	private final BigDecimal sum;
	private final BigDecimal monthsAmount;

	@JsonCreator
	public CreditRequestDto(@JsonProperty("client") ClientDto client, @JsonProperty("creditTypeId") Long creditTypeId, @JsonProperty("creditorId") Long creditorId,
			@JsonProperty("sum") BigDecimal sum, @JsonProperty("monthsAmount") BigDecimal monthsAmount) {

		this.client = client;
		this.creditTypeId = creditTypeId;
		this.creditorId = creditorId;
		this.sum = sum;
		this.monthsAmount = monthsAmount;
	}

	public ClientDto getClient() {

		return client;
	}

	public Long getCreditTypeId() {

		return creditTypeId;
	}

	public Long getCreditorId() {

		return creditorId;
	}

	public BigDecimal getSum() {

		return sum;
	}

	public BigDecimal getMonthsAmount() {

		return monthsAmount;
	}
}
