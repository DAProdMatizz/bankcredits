package com.bank.credits.dto.credit;

import com.bank.credits.dto.annotation.JsonRounding;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CreditTypeDto {

	private final Long id;
	private final String name;
	private final BigDecimal rate;
	private final Integer minSum;
	private final Integer minMonths;

	@JsonCreator
	public CreditTypeDto(@JsonProperty("id") Long id, @JsonProperty("name") String name, @JsonRounding @JsonProperty("rate") BigDecimal rate,
			@JsonProperty("minSum") Integer minSum, @JsonProperty("minMonths") Integer minMonths) {

		this.id = id;
		this.name = name;
		this.rate = rate;
		this.minSum = minSum;
		this.minMonths = minMonths;
	}

	public Long getId() {

		return id;
	}

	public String getName() {

		return name;
	}

	public BigDecimal getRate() {

		return rate;
	}

	public Integer getMinSum() {

		return minSum;
	}

	public Integer getMinMonths() {

		return minMonths;
	}
}
