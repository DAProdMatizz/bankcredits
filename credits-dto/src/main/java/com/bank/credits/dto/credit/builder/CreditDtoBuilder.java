package com.bank.credits.dto.credit.builder;

import com.bank.credits.dto.client.ClientDto;
import com.bank.credits.dto.credit.CreditDto;
import com.bank.credits.dto.credit.CreditTypeDto;
import com.bank.credits.dto.creditor.CreditorDto;
import com.bank.credits.dto.repayment.RepaymentDto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class CreditDtoBuilder {

	private Long id;
	private ClientDto client;
	private CreditTypeDto creditType;
	private CreditorDto creditor;
	private BigDecimal number;
	private boolean payedOff;
	private Date openingDate;
	private BigDecimal fullSum;
	private BigDecimal monthsAmount;
	private List<RepaymentDto> repayments;

	private CreditDtoBuilder() {

	}

	public static CreditDtoBuilder newBuilder() {

		return new CreditDtoBuilder();
	}

	public CreditDtoBuilder id(Long id) {

		this.id = id;
		return this;
	}

	public CreditDtoBuilder client(ClientDto client) {

		this.client = client;
		return this;
	}

	public CreditDtoBuilder creditType(CreditTypeDto creditType) {

		this.creditType = creditType;
		return this;
	}

	public CreditDtoBuilder creditor(CreditorDto creditor) {

		this.creditor = creditor;
		return this;
	}

	public CreditDtoBuilder number(BigDecimal number) {

		this.number = number;
		return this;
	}

	public CreditDtoBuilder payedOff(boolean payedOff) {

		this.payedOff = payedOff;
		return this;
	}

	public CreditDtoBuilder openingDate(Date openingDate) {

		this.openingDate = openingDate;
		return this;
	}

	public CreditDtoBuilder fullSum(BigDecimal fullSum) {

		this.fullSum = fullSum;
		return this;
	}

	public CreditDtoBuilder monthsAmount(BigDecimal monthsAmount) {

		this.monthsAmount = monthsAmount;
		return this;
	}

	public CreditDtoBuilder repayments(List<RepaymentDto> repayments) {

		this.repayments = repayments;
		return this;
	}

	public CreditDto build() {

		return new CreditDto(id, client, creditType, creditor, number, payedOff, openingDate, fullSum, monthsAmount, repayments);
	}
}
