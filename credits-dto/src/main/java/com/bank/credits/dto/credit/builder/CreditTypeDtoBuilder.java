package com.bank.credits.dto.credit.builder;

import com.bank.credits.dto.credit.CreditTypeDto;
import java.math.BigDecimal;

public class CreditTypeDtoBuilder {

	private Long id;
	private String name;
	private BigDecimal rate;
	private Integer minSum;
	private Integer minMonths;

	private CreditTypeDtoBuilder() {

	}

	public static CreditTypeDtoBuilder newBuilder() {

		return new CreditTypeDtoBuilder();
	}

	public CreditTypeDtoBuilder id(Long id) {

		this.id = id;
		return this;
	}

	public CreditTypeDtoBuilder name(String name) {

		this.name = name;
		return this;
	}

	public CreditTypeDtoBuilder rate(BigDecimal rate) {

		this.rate = rate;
		return this;
	}

	public CreditTypeDtoBuilder minSum(Integer minSum) {

		this.minSum = minSum;
		return this;
	}

	public CreditTypeDtoBuilder minMonths(Integer minMonths) {

		this.minMonths = minMonths;
		return this;
	}

	public CreditTypeDto build() {

		return new CreditTypeDto(id, name, rate, minSum, minMonths);
	}
}
