package com.bank.credits.dto.creditor.builder;

import com.bank.credits.dto.creditor.CreditorDto;

public class CreditorDtoBuilder {

	private Long id;
	private String name;

	private CreditorDtoBuilder() {

	}

	public static CreditorDtoBuilder newBuilder() {

		return new CreditorDtoBuilder();
	}

	public CreditorDtoBuilder id(Long id) {

		this.id = id;
		return this;
	}

	public CreditorDtoBuilder name(String name) {

		this.name = name;
		return this;
	}

	public CreditorDto build() {

		return new CreditorDto(id, name);
	}
}
