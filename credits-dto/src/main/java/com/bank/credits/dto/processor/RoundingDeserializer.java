package com.bank.credits.dto.processor;

import com.bank.credits.common.exception.RoundingDeserializationException;
import com.bank.credits.common.exception.RoundingNumberException;
import com.bank.credits.dto.annotation.JsonRounding;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Objects;

public class RoundingDeserializer<T> extends JsonDeserializer<T> implements ContextualDeserializer {

	private final int precision;
	private final boolean useRounding;
	private final JavaType javaType;

	private RoundingDeserializer() {

		this(2, true, null);
	}

	private RoundingDeserializer(int precision, boolean useRounding, JavaType type) {

		this.precision = precision;
		this.useRounding = useRounding;
		this.javaType = type;
	}

	@Override
	@SuppressWarnings("unchecked")
	public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

		var value = jsonParser.getCodec().readValue(jsonParser, javaType);

		if (Objects.isNull(value)) {
			return null;
		}

		if (!useRounding) {
			return (T) value;
		}

		if (value instanceof Number) {
			return (T) roundValue((Number) value);
		}

		if (javaType.isCollectionLikeType() && value instanceof Collection) {
			try {
				Collection roundedCollection = (Collection) value.getClass().getConstructor().newInstance();
				((Collection) value).forEach(item -> {
					Number roundedValue = roundValue((Number) item);
					roundedCollection.add(roundedValue);
				});
				return (T) roundedCollection;
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
				throw new RoundingDeserializationException(e, "can't deserialize collection");
			}
		}

		throw new RoundingDeserializationException("Unsupported deserialization type: " + value.getClass().getName());
	}

	@SuppressWarnings("unchecked")
	private <N extends Number> N roundValue(Number value) {

		if (value instanceof BigDecimal) {
			return (N) ((BigDecimal) value).setScale(precision, RoundingMode.HALF_UP);
		} else if (value instanceof Double) {
			Double doubleValue = BigDecimal.valueOf(value.doubleValue()).setScale(precision, RoundingMode.HALF_UP).doubleValue();
			return (N) doubleValue;
		} else if (value instanceof Float) {
			Float floatValue = BigDecimal.valueOf(value.floatValue()).setScale(precision, RoundingMode.HALF_UP).floatValue();
			return (N) floatValue;
		}

		throw new RoundingNumberException(value);
	}

	@Override
	public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty property) {

		JsonRounding annotation = property.getAnnotation(JsonRounding.class);
		if (Objects.nonNull(annotation)) {
			JavaType type = property.getType();
			int precision = annotation.precision();
			boolean useRounding = annotation.useBigDecimalRoundingDeserializer();
			return new RoundingDeserializer(precision, useRounding, type);
		}
		return this;
	}
}
