package com.bank.credits.dto.processor;

import com.bank.credits.common.exception.JsonGenerationException;
import com.bank.credits.common.exception.RoundingNumberException;
import com.bank.credits.dto.annotation.JsonRounding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Objects;

public class RoundingSerializer<T> extends JsonSerializer<T> implements ContextualSerializer {

	private final int precision;

	private RoundingSerializer() {

		this(3);
	}

	private RoundingSerializer(int precision) {

		this.precision = precision;
	}

	@Override
	public void serialize(T value, JsonGenerator gen, SerializerProvider serializers) throws IOException {

		if (Objects.isNull(value)) {
			gen.writeNull();
		} else {

			var clazz = value.getClass();
			if (Collection.class.isAssignableFrom(clazz)) {

				@SuppressWarnings("unchecked") var collection = (Collection<Number>) value;
				gen.writeStartArray();
				collection.forEach(number -> writeQuietly(gen, number));
				gen.writeEndArray();
			} else {
				writeQuietly(gen, (Number) value);
			}
		}
	}

	@Override
	public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) {

		JsonRounding annotation = property.getAnnotation(JsonRounding.class);
		if (Objects.nonNull(annotation)) {
			int precision = annotation.precision();
			return new RoundingSerializer<T>(precision);
		}
		return this;
	}

	private void writeQuietly(JsonGenerator gen, Number number) {

		try {
			if (number instanceof BigDecimal) {
				BigDecimal rounded = ((BigDecimal) number).setScale(precision, RoundingMode.HALF_UP);
				gen.writeNumber(rounded);
			} else if (number instanceof Double || number instanceof Float) {
				BigDecimal rounded = (BigDecimal.valueOf(number.doubleValue()).setScale(precision, RoundingMode.HALF_UP));
				gen.writeNumber(rounded);
			} else {
				throw new RoundingNumberException(number);
			}
		} catch (Exception e) {
			throw new JsonGenerationException(e);
		}
	}
}
