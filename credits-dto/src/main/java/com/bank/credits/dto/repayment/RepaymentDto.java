package com.bank.credits.dto.repayment;

import com.bank.credits.common.utils.DateUtils;
import com.bank.credits.dto.annotation.JsonRounding;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;

public class RepaymentDto {

	private final Long id;
	private final Date repaymentDate;
	private final BigDecimal repaymentSum;

	@JsonCreator
	public RepaymentDto(@JsonProperty("id") Long id, @JsonFormat(pattern = DateUtils.APPLICATION_DATE_PATTERN) @JsonProperty("repaymentDate") Date repaymentDate,
			@JsonRounding @JsonProperty("repaymentSum") BigDecimal repaymentSum) {

		this.id = id;
		this.repaymentDate = repaymentDate;
		this.repaymentSum = repaymentSum;
	}

	public Long getId() {

		return id;
	}

	public Date getRepaymentDate() {

		return repaymentDate;
	}

	public BigDecimal getRepaymentSum() {

		return repaymentSum;
	}
}
