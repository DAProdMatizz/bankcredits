package com.bank.credits.dto.repayment;

import com.bank.credits.dto.credit.CreditDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class RepaymentRequestDto {

	private final BigDecimal paymentSum;
	private final CreditDto credit;

	@JsonCreator
	public RepaymentRequestDto(@JsonProperty("paymentSum") BigDecimal paymentSum, @JsonProperty("credit") CreditDto credit) {

		this.paymentSum = paymentSum;
		this.credit = credit;
	}

	public BigDecimal getPaymentSum() {

		return paymentSum;
	}

	public CreditDto getCredit() {

		return credit;
	}
}
