package com.bank.credits.dto.repayment.builder;

import com.bank.credits.dto.repayment.RepaymentDto;

import java.math.BigDecimal;
import java.util.Date;

public class RepaymentDtoBuilder {

	private Long id;
	private Date repaymentDate;
	private BigDecimal repaymentSum;

	private RepaymentDtoBuilder() {

	}

	public static RepaymentDtoBuilder newBuilder() {

		return new RepaymentDtoBuilder();
	}

	public RepaymentDtoBuilder id(Long id) {

		this.id = id;
		return this;
	}

	public RepaymentDtoBuilder repaymentDate(Date repaymentDate) {

		this.repaymentDate = repaymentDate;
		return this;
	}

	public RepaymentDtoBuilder repaymentSum(BigDecimal sum) {

		this.repaymentSum = sum;
		return this;
	}

	public RepaymentDto build() {

		return new RepaymentDto(id, repaymentDate, repaymentSum);
	}
}
