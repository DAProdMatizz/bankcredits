import {Component, OnInit} from '@angular/core';

import {AuthService} from "../../../internal/shared/service/auth/auth.service";

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  onLogOutClick() {

    this.authService.logout();
  }
}
