import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {AdminComponent} from "./admin.component";
import {AdminGuard} from "../../internal/core/guard/admin-guard";
import {CreditComponent} from "./credit/credit.component";
import {ClientComponent} from "./client/client.component";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AdminGuard], children: [
      {path: 'credits', component: CreditComponent},
      {path: 'clients', component: ClientComponent}
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
