import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminHeaderComponent} from "./admin-header/admin-header.component";
import {ClientComponent} from "./client/client.component";
import {CreditComponent} from "./credit/credit.component";
import {AdminComponent} from "./admin.component";
import {AdminRoutingModule} from "./admin-routing.module";

@NgModule({
  declarations: [
    AdminHeaderComponent,
    ClientComponent,
    CreditComponent,
    AdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
