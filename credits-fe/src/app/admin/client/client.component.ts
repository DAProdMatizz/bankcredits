import {Component, OnInit} from '@angular/core';

import {ClientDto} from "../../../internal/shared/dto/client/client.dto";
import {ClientService} from "../../../internal/shared/service/client/client.service";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clients: ClientDto[];

  constructor(private clientService: ClientService) {
  }

  ngOnInit(): void {

    this.clientService.loadClients().subscribe(clients => {
      this.clients = clients;
    });
  }
}
