import {Component, OnInit} from '@angular/core';

import {CreditDto} from "../../../internal/shared/dto/credit/credit.dto";
import {CreditService} from "../../../internal/shared/service/credit/credit.service";
import {CreditUtils} from "../../../internal/shared/utils/credit-utils";

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

  credits: CreditDto[];

  constructor(private creditService: CreditService) {
  }

  ngOnInit(): void {

    this.creditService.loadCredits().subscribe(credits => {
      this.credits = credits;
    });
  }

  clientFullName(credit: CreditDto): string {

    const firstName = credit.client.firstName;
    const secondName = credit.client.secondName;

    return firstName + ' ' + secondName;
  }

  isCreditPayedOff(credit: CreditDto): string {

    const creditPayedOff = credit.payedOff;
    return creditPayedOff ? CreditUtils.PAYED : CreditUtils.IN_PROCESS;
  }
}
