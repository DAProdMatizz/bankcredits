import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../internal/core/core.module";
import {LoginModule} from "./login/login.module";
import {UserModule} from "./user/user.module";
import {AdminModule} from "./admin/admin.module";
import {StoreModule} from '@ngrx/store';
import {appReducerMap} from "../internal/core/store/reducer/app-reducer-map";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(appReducerMap),
    CoreModule,
    LoginModule,
    UserModule,
    AdminModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
