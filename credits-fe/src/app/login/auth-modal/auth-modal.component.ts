import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

import {AuthService} from "../../../internal/shared/service/auth/auth.service";
import {AuthUtils} from "../../../internal/shared/utils/auth-utils";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.css'],
  animations: [
    trigger('modalState', [
      state('end', style({
        transform: 'translateY(15vh)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          transform: 'translateY(0)',
          opacity: 0
        }),
        animate(300)
      ])
    ])
  ]
})
export class AuthModalComponent implements OnInit {

  @Output() closeModal = new EventEmitter<void>();
  authForm: FormGroup;
  error: string;
  modalState: string = 'end';

  constructor(private router: Router, private authService: AuthService) {

  }

  ngOnInit(): void {

    this.authForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  onLogin(): void {

    this.authService.login(this.authForm.value).subscribe(user => {
      const path = AuthUtils.navigationPathByRole(user.role);
      this.router.navigate([path]).then();
    }, errorMessage => {
      this.error = errorMessage;
    });
  }

  onClose(): void {

    this.closeModal.emit();
  }
}
