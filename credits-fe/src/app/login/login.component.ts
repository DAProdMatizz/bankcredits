import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";

import {AuthPlaceholderDirective} from "../../internal/core/directive/auth-placeholder.directive";
import {AuthModalComponent} from "./auth-modal/auth-modal.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  @ViewChild(AuthPlaceholderDirective, {static: false}) loginHost: AuthPlaceholderDirective;
  loginProcess: boolean;

  private closeAuthModalSubscription: Subscription;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {

    this.loginProcess = false;
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

    if (this.closeAuthModalSubscription) {
      this.closeAuthModalSubscription.unsubscribe();
    }
  }

  onLogin(): void {

    this.loginProcess = true;
    this.showLoginForm();
  }

  private showLoginForm(): void {

    const authModalComponentFactory = this.componentFactoryResolver.resolveComponentFactory(AuthModalComponent);
    const loginContainerRef = this.loginHost.viewContainerRef;
    loginContainerRef.clear();

    const authModalComponentRef = loginContainerRef.createComponent(authModalComponentFactory);
    this.closeAuthModalSubscription = authModalComponentRef.instance.closeModal.subscribe(() => {
      this.closeAuthModalSubscription.unsubscribe();
      loginContainerRef.clear();
      this.loginProcess = false;
    });
  }
}
