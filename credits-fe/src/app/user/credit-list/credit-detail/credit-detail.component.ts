import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {CreditDto} from "../../../../internal/shared/dto/credit/credit.dto";
import {CreditService} from "../../../../internal/shared/service/credit/credit.service";
import {CreditUtils} from "../../../../internal/shared/utils/credit-utils";
import {RouterUtils} from "../../../../internal/shared/utils/router-utils";
import {map, switchMap} from "rxjs/internal/operators";
import {NewPaymentPlaceholderDirective} from "../../../../internal/core/directive/new-payment-placeholder.directive";
import {Subscription} from "rxjs";
import {NewPaymentModalComponent} from "./new-payment-modal/new-payment-modal.component";

@Component({
  selector: 'app-credit-detail',
  templateUrl: './credit-detail.component.html',
  styleUrls: ['./credit-detail.component.css']
})
export class CreditDetailComponent implements OnInit, OnDestroy {

  private readonly ID_NAME = 'id';

  private creditId: number;
  private closeNewPaymentModalSubscription: Subscription;

  @ViewChild(NewPaymentPlaceholderDirective, {static: false}) newPaymentHost: NewPaymentPlaceholderDirective;
  credit: CreditDto;

  constructor(private creditService: CreditService, private router: Router, private route: ActivatedRoute, private componentFactoryResolver: ComponentFactoryResolver) {

    this.credit = CreditUtils.createEmptyCreditDto();
  }

  ngOnInit(): void {

    this.route.params.pipe(
      map(params => {
        return Number(params[this.ID_NAME]);
      }), switchMap(id => {
        this.creditId = id;
        return this.creditService.loadCreditById(id);
      })
    ).subscribe(credit => {
      if (credit) {
        this.credit = credit;
      } else {
        this.router.navigate([RouterUtils.ONE_LEVEL_BACK], {relativeTo: this.route}).then();
      }
    });
  }

  ngOnDestroy(): void {

    if (this.closeNewPaymentModalSubscription) {
      this.closeNewPaymentModalSubscription.unsubscribe();
    }
  }

  isCreditPayedOff(): string {

    const creditPayedOff = this.credit.payedOff;
    return creditPayedOff ? CreditUtils.PAYED : CreditUtils.IN_PROCESS;
  }

  getCreditDuration(): string {

    const months = this.credit.monthsAmount;
    if (months === 1) {
      return '1 month';
    } else {
      return `${months} months`;
    }
  }

  getCreditRate(): string {

    const rateInPercent: number = this.credit.creditType.rate * CreditUtils.ONE_HUNDRED;
    return `${rateInPercent}%`;
  }

  onNewPayment(): void {

    const newPaymentModalComponentFactory = this.componentFactoryResolver.resolveComponentFactory(NewPaymentModalComponent);
    const newPaymentContainerRef = this.newPaymentHost.viewContainerRef;
    newPaymentContainerRef.clear();

    const newPaymentModalComponentRef = newPaymentContainerRef.createComponent(newPaymentModalComponentFactory);
    const newPaymentModalInstance = newPaymentModalComponentRef.instance;

    newPaymentModalInstance.credit = this.credit;
    this.closeNewPaymentModalSubscription = newPaymentModalInstance.closeModal.subscribe(() => {
      this.closeNewPaymentModalSubscription.unsubscribe();
      newPaymentContainerRef.clear();
    });
  }
}
