import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RepaymentChartComponent} from "./repayment-chart/repayment-chart.component";
import {CreditDetailComponent} from "./credit-detail.component";
import {NgApexchartsModule} from "ng-apexcharts";
import {CoreModule} from "../../../../internal/core/core.module";
import { NewPaymentModalComponent } from './new-payment-modal/new-payment-modal.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    RepaymentChartComponent,
    CreditDetailComponent,
    NewPaymentModalComponent
  ],
  imports: [
    CommonModule,
    NgApexchartsModule,
    CoreModule,
    ReactiveFormsModule
  ]
})
export class CreditDetailModule { }
