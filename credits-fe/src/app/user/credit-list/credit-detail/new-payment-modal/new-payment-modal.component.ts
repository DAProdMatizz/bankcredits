import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {CreditDto} from "../../../../../internal/shared/dto/credit/credit.dto";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {RepaymentService} from "../../../../../internal/shared/service/repayment/repayment.service";
import {RouterUtils} from "../../../../../internal/shared/utils/router-utils";

@Component({
  selector: 'app-new-payment-modal',
  templateUrl: './new-payment-modal.component.html',
  styleUrls: ['./new-payment-modal.component.css'],
  animations: [
    trigger('modalState', [
      state('end', style({
        transform: 'translateY(15vh)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          transform: 'translateY(0)',
          opacity: 0
        }),
        animate(300)
      ])
    ])
  ]
})
export class NewPaymentModalComponent implements OnInit {

  @Output() closeModal = new EventEmitter<void>();
  paymentForm: FormGroup;
  credit: CreditDto;
  modalState: string = 'end';
  error: string;

  constructor(private router: Router, private repaymentService: RepaymentService) {

  }

  ngOnInit(): void {

    this.paymentForm = new FormGroup({
      paymentSum: new FormControl(null, Validators.required)
    });
  }

  onNewPayment(): void {

    this.repaymentService.addPayment(this.paymentForm.value, this.credit).subscribe(paymentNumber => {
      this.router.navigate([RouterUtils.CREDITS]).then();
    }, errorMessage => {
      this.error = errorMessage;
    });
  }

  onClose(): void {

    this.closeModal.emit();
  }
}
