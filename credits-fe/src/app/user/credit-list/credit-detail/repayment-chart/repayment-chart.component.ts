import {Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {ApexChart, ApexNonAxisChartSeries, ApexResponsive, ApexTheme, ChartComponent} from "ng-apexcharts";
import {Subscription} from "rxjs";

import {RepaymentDto} from "../../../../../internal/shared/dto/repayment/repayment.dto";
import {CreditDto} from "../../../../../internal/shared/dto/credit/credit.dto";
import {CreditService} from "../../../../../internal/shared/service/credit/credit.service";
import {CreditUtils} from "../../../../../internal/shared/utils/credit-utils";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  theme: ApexTheme;
};

@Component({
  selector: 'app-repayment-chart',
  templateUrl: './repayment-chart.component.html',
  styleUrls: ['./repayment-chart.component.css']
})
export class RepaymentChartComponent implements OnInit, OnDestroy {

  creditInitializedSubscription: Subscription;

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private creditService: CreditService) {
    this.chartOptions = {
      series: [25, 75],
      chart: {
        width: 400,
        type: "pie"
      },
      labels: ["Payed Off", "Left to pay"],
      theme: {
        monochrome: {
          enabled: true,
          color: "#8728d0",
          shadeTo: 'light',
          shadeIntensity: 0.65
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
  }

  ngOnInit(): void {

    this.creditInitializedSubscription = this.creditService.creditInitialized.subscribe(credit => {
      this.chartOptions.labels = this.buildLabels(credit);
      this.chartOptions.series = this.buildSeries(credit);
    });
  }

  ngOnDestroy(): void {

    this.creditInitializedSubscription.unsubscribe();
  }

  buildSeries(credit: CreditDto): ApexNonAxisChartSeries {

    if (credit.repayments === null) {
      return [100];
    }

    const paymentSeries = credit.repayments.map((repayment: RepaymentDto) => {
      return CreditUtils.calculatePaymentPercent(repayment.repaymentSum, credit.fullSum);
    });

    const repaymentSum = CreditUtils.calculateCreditRepaymentSum(credit);
    const leftToPaySum = credit.fullSum - repaymentSum;

    if (leftToPaySum > 0) {
      const leftToPaySumInPercent = CreditUtils.calculatePaymentPercent(leftToPaySum, credit.fullSum);
      paymentSeries.push(leftToPaySumInPercent);
    }
    return paymentSeries;
  }

  buildLabels(credit: CreditDto): string[] {

    if (credit.repayments === null) {
      return [CreditUtils.LEFT_TO_PAY];
    }

    const paymentLabels = credit.repayments.map((repayment: RepaymentDto) => {
      return (repayment.repaymentDate);
    });

    const repaymentSum = CreditUtils.calculateCreditRepaymentSum(credit);
    const leftToPaySum = credit.fullSum - repaymentSum;

    if (leftToPaySum > 0) {
      paymentLabels.push(CreditUtils.LEFT_TO_PAY);
    }

    return paymentLabels;
  }
}
