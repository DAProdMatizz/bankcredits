import {Component, Input, OnInit} from '@angular/core';

import {CreditDto} from "../../../../internal/shared/dto/credit/credit.dto";
import {CreditUtils} from "../../../../internal/shared/utils/credit-utils";

@Component({
  selector: 'app-credit-elem',
  templateUrl: './credit-elem.component.html',
  styleUrls: ['./credit-elem.component.css']
})
export class CreditElemComponent implements OnInit {

  @Input() credit: CreditDto;
  repaymentPercent: number;

  constructor() {
  }

  ngOnInit(): void {

    this.repaymentPercent = this.getRepaymentPercent();
  }

  getRepaymentPercent(): number {

    return CreditUtils.calculateRepaymentSumInPercent(this.credit);
  }
}
