import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RepaymentRadialbarComponent} from "./repayment-radialbar/repayment-radialbar.component";
import {CreditElemComponent} from "./credit-elem.component";
import {NgApexchartsModule} from "ng-apexcharts";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    RepaymentRadialbarComponent,
    CreditElemComponent,
  ],
  exports: [
    CreditElemComponent
  ],
  imports: [
    CommonModule,
    NgApexchartsModule,
    RouterModule
  ]
})
export class CreditElemModule {
}
