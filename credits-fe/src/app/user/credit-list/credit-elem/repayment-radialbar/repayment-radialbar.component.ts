import {Component, Input, OnInit, ViewChild} from "@angular/core";

import {ApexChart, ApexNonAxisChartSeries, ApexPlotOptions, ApexStroke, ChartComponent} from "ng-apexcharts";
import {ApexFill} from "ng-apexcharts/lib/model/apex-types";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  fill: ApexFill;
  stroke: ApexStroke;
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'app-repayment-radialbar',
  templateUrl: './repayment-radialbar.component.html',
  styleUrls: ['./repayment-radialbar.component.css']
})
export class RepaymentRadialbarComponent implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  @Input() repaymentPercent: number;

  public chartOptions: Partial<ChartOptions>;

  constructor() {

    this.chartOptions = {
      chart: {
        height: 180,
        type: "radialBar",
      },
      series: [0],
      plotOptions: {
        radialBar: {
          hollow: {
            margin: 0,
            size: "70%",
            background: "rgb(139,4,193)"
          },
          dataLabels: {
            name: {
              offsetY: -10,
              color: "#fff",
              fontSize: "10px"
            },
            value: {
              color: "#fff",
              fontSize: "20px",
              show: true
            }
          }
        }
      },
      fill: {
        colors: ["#985fc9"]
      },
      stroke: {
        lineCap: "round"
      },
      labels: ["Payment progress"]
    };
  }

  ngOnInit(): void {

    this.chartOptions.series = [this.repaymentPercent];
  }
}
