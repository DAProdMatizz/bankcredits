import {RouterModule, Routes} from "@angular/router";
import {CreditListComponent} from "./credit-list.component";
import {ClientGuard} from "../../../internal/core/guard/client-guard";
import {CreditDetailComponent} from "./credit-detail/credit-detail.component";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

const routes: Routes = [
  {
    path: '', component: CreditListComponent, canActivate: [ClientGuard], children: [
      {
        path: ':id', component: CreditDetailComponent
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CreditListRoutingModule {
}
