import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";

import {CreditDto} from "../../../internal/shared/dto/credit/credit.dto";
import {CreditService} from "../../../internal/shared/service/credit/credit.service";
import {CreditUtils} from "../../../internal/shared/utils/credit-utils";
import {Subscription} from "rxjs";
import {NewCreditModalComponent} from "./new-credit-modal/new-credit-modal.component";
import {NewCreditPlaceholderDirective} from "../../../internal/core/directive/new-credit-placeholder.directive";

@Component({
  selector: 'app-credit-list',
  templateUrl: './credit-list.component.html',
  styleUrls: ['./credit-list.component.css']
})
export class CreditListComponent implements OnInit, OnDestroy {

  @ViewChild(NewCreditPlaceholderDirective, {static: false}) newCreditHost: NewCreditPlaceholderDirective;
  credits: CreditDto[];

  private closeNewCreditModalSubscription: Subscription;

  constructor(private router: Router, private creditService: CreditService, private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {

    this.creditService.loadClientCredits().subscribe(credits => {
      this.credits = credits.map(credit => {
        const repaymentSum = this.getCreditRepaymentsSum(credit);
        return {repaymentSum: repaymentSum, ...credit};
      });
    });
  }

  ngOnDestroy(): void {

    if (this.closeNewCreditModalSubscription) {
      this.closeNewCreditModalSubscription.unsubscribe();
    }
  }

  getCreditRepaymentsSum(credit: CreditDto): number {

    return CreditUtils.calculateCreditRepaymentSum(credit);
  }

  onNewCredit(): void {

    this.showNewCreditForm();
  }

  private showNewCreditForm(): void {

    const newCreditModalComponentFactory = this.componentFactoryResolver.resolveComponentFactory(NewCreditModalComponent);
    const newCreditContainerRef = this.newCreditHost.viewContainerRef;
    newCreditContainerRef.clear();

    const newCreditModalComponentRef = newCreditContainerRef.createComponent(newCreditModalComponentFactory);
    this.closeNewCreditModalSubscription = newCreditModalComponentRef.instance.closeModal.subscribe(() => {
      this.closeNewCreditModalSubscription.unsubscribe();
      newCreditContainerRef.clear();
    });
  }
}
