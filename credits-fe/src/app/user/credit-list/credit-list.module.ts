import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreditListComponent} from "./credit-list.component";
import {CreditElemModule} from "./credit-elem/credit-elem.module";
import {CreditDetailModule} from "./credit-detail/credit-detail.module";
import {CreditListRoutingModule} from "./credit-list-routing.module";
import {NewCreditModalComponent} from "./new-credit-modal/new-credit-modal.component";
import {ReactiveFormsModule} from "@angular/forms";
import {CoreModule} from "../../../internal/core/core.module";

@NgModule({
  declarations: [
    CreditListComponent,
    NewCreditModalComponent
  ],
  imports: [
    CommonModule,
    CreditListRoutingModule,
    CreditElemModule,
    CreditDetailModule,
    ReactiveFormsModule,
    CoreModule
  ]
})
export class CreditListModule {
}
