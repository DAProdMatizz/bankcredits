import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {CreditTypeDto} from "../../../../internal/shared/dto/credit-type/credit-type.dto";
import {CreditorDto} from "../../../../internal/shared/dto/creditor/creditor.dto";
import {CreditTypeService} from "../../../../internal/shared/service/credit-type/credit-type.service";
import {CreditorService} from "../../../../internal/shared/service/creditor/creditor.service";
import {CreditService} from "../../../../internal/shared/service/credit/credit.service";
import {Router} from "@angular/router";
import {RouterUtils} from "../../../../internal/shared/utils/router-utils";

@Component({
  selector: 'app-new-credit',
  templateUrl: './new-credit-modal.component.html',
  styleUrls: ['./new-credit-modal.component.css'],
  animations: [
    trigger('modalState', [
      state('end', style({
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0
        }),
        animate(200)
      ])
    ])
  ]
})
export class NewCreditModalComponent implements OnInit {

  @Output() closeModal = new EventEmitter<void>();
  creditForm: FormGroup;
  modalState: string = 'end';

  creditTypes: CreditTypeDto[];
  creditors: CreditorDto[];
  error: string;

  constructor(private router: Router, private creditService: CreditService, private creditTypeService: CreditTypeService, private creditorService: CreditorService) {

  }

  ngOnInit(): void {

    this.creditForm = new FormGroup({
      creditTypeId: new FormControl(null, Validators.required),
      creditorId: new FormControl(null, Validators.required),
      sum: new FormControl(null, Validators.required),
      monthsAmount: new FormControl(null, Validators.required)
    });

    this.creditTypeService.loadCreditTypes().subscribe(creditTypes => {
      this.creditTypes = creditTypes;
    });

    this.creditorService.loadCreditors().subscribe(creditors => {
      this.creditors = creditors;
    });
  }

  onCreateCredit(): void {

    this.creditService.createCredit(this.creditForm.value).subscribe(newCreditId => {
      this.router.navigate([RouterUtils.CREDITS]).then();
    }, errorMessage => {
      this.error = errorMessage;
    });
  }

  onClose(): void {

    this.closeModal.emit();
  }
}
