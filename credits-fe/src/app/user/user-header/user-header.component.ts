import {Component, OnInit} from '@angular/core';

import {AuthService} from "../../../internal/shared/service/auth/auth.service";

@Component({
  selector: 'app-user-header',
  templateUrl: './user-header.component.html',
  styleUrls: ['./user-header.component.css']
})
export class UserHeaderComponent implements OnInit {

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  onLogOutClick(): void {

    this.authService.logout();
  }
}
