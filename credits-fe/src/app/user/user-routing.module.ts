import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "./user.component";

const routes: Routes = [
  {
    path: '', component: UserComponent, children: [
      {path: 'credits', loadChildren: () => import('./credit-list/credit-list.module').then(module => module.CreditListModule)}
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule {
}
