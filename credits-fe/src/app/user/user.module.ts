import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserHeaderComponent} from "./user-header/user-header.component";
import {UserComponent} from "./user.component";
import {UserRoutingModule} from "./user-routing.module";
import {CreditListModule} from "./credit-list/credit-list.module";

@NgModule({
  declarations: [
    UserHeaderComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    CreditListModule
  ]
})
export class UserModule {
}
