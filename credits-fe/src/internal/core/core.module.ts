import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "./interceptor/auth-interceptor";
import {ErrorInterceptor} from "./interceptor/error-interceptor";
import {AuthPlaceholderDirective} from './directive/auth-placeholder.directive';
import {NewCreditPlaceholderDirective} from './directive/new-credit-placeholder.directive';
import { NewPaymentPlaceholderDirective } from './directive/new-payment-placeholder.directive';

@NgModule({
  declarations: [AuthPlaceholderDirective, NewCreditPlaceholderDirective, NewPaymentPlaceholderDirective],
  imports: [
    CommonModule
  ],
  exports: [
    AuthPlaceholderDirective,
    NewCreditPlaceholderDirective,
    NewPaymentPlaceholderDirective
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ]
})
export class CoreModule {
}
