import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appAuthPlaceholder]'
})
export class AuthPlaceholderDirective {

  constructor(public viewContainerRef: ViewContainerRef) {

  }
}
