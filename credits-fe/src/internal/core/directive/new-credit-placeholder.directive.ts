import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appNewCreditPlaceholder]'
})
export class NewCreditPlaceholderDirective {

  constructor(public viewContainerRef: ViewContainerRef) {

  }
}
