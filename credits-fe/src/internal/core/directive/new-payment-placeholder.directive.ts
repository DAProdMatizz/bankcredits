import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appNewPaymentPlaceholder]'
})
export class NewPaymentPlaceholderDirective {

  constructor(public viewContainerRef: ViewContainerRef) {

  }
}
