import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {map, take} from "rxjs/operators";
import {RouterUtils} from "../../shared/utils/router-utils";
import {AuthUtils} from "../../shared/utils/auth-utils";
import {Store} from "@ngrx/store";
import {AppState} from "../store/state/app.state";
import {StateUtils} from "../store/state/state.utils";

@Injectable({
  providedIn: "root"
})
export class AdminGuard implements CanActivate {

  constructor(private router: Router, private store: Store<AppState>) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.store.select(StateUtils.AUTH).pipe(
      take(1),
      map(authState => {
        const user = authState.loggedUser;
        const isLogged: boolean = !!user;

        if (isLogged) {
          return user.role === AuthUtils.ADMIN_ROLE;
        }

        return this.router.createUrlTree([RouterUtils.LOGIN]);
      })
    );
  }
}
