import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {exhaustMap, map, take} from "rxjs/operators";
import {AuthUtils} from "../../shared/utils/auth-utils";
import {Injectable} from "@angular/core";
import {Store} from '@ngrx/store';
import {AppState} from "../store/state/app.state";
import {StateUtils} from "../store/state/state.utils";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store<AppState>) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return this.store.select(StateUtils.AUTH).pipe(
      take(1),
      map(authState => {
        return authState.loggedUser;
      }),
      exhaustMap(user => {
        if (!user) {
          return next.handle(request);
        }
        const modifiedRequest = request.clone({headers: AuthUtils.buildAuthorizationHeader(user.token)});
        return next.handle(modifiedRequest);
      })
    );
  }
}
