import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {AuthService} from "../../shared/service/auth/auth.service";
import {catchError} from "rxjs/operators";
import {AuthUtils} from "../../shared/utils/auth-utils";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(catchError(error => {
      if (error.status === AuthUtils.HTTP_STATUS_UNAUTHORIZED) {
        this.authService.logout();
      }
      return throwError(error);
    }));
  }
}
