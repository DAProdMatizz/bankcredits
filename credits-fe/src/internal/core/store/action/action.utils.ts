export class ActionUtils {

  public static readonly USER_LOGGED_IN = 'USER_LOGGED_IN';
  public static readonly USER_LOGGED_OUT = 'USER_LOGGED_OUT';
}
