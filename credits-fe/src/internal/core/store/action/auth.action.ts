import { Action } from '@ngrx/store';
import {UserDto} from "../../../shared/dto/authentication/user.dto";

export interface AuthAction extends Action {

  readonly type: string;
  payload?: UserDto;
}
