import {ActionUtils} from "./action.utils";
import {UserDto} from "../../../shared/dto/authentication/user.dto";
import {AuthAction} from "./auth.action";

export class LoginAction implements AuthAction {

  readonly type: string = ActionUtils.USER_LOGGED_IN;

  constructor(public payload: UserDto) {

  }
}
