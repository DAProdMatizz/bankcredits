import {AuthAction} from "./auth.action";
import {ActionUtils} from "./action.utils";

export class LogoutAction implements AuthAction {

  readonly type: string = ActionUtils.USER_LOGGED_OUT;
}
