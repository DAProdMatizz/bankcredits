import {ActionReducerMap} from '@ngrx/store';
import {AppState} from "../state/app.state";
import {authReducer} from "./auth.reducer";

export const appReducerMap: ActionReducerMap<AppState> = {
  auth: authReducer
}
