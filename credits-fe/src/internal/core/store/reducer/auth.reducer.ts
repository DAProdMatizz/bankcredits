import {ActionUtils} from "../action/action.utils";
import {AuthAction} from "../action/auth.action";
import {AuthState} from "../state/auth.state";

const initialState: AuthState = {
  loggedUser: null
};

export function authReducer(state: AuthState = initialState, action: AuthAction): AuthState {

  const actionType = action.type;

  if (actionType === ActionUtils.USER_LOGGED_IN) {
    return userLoggedIn(state, action);
  } else if (actionType === ActionUtils.USER_LOGGED_OUT) {
    return userLoggedOut(state);
  } else {
    return state;
  }
}

function userLoggedIn(state: AuthState, action: AuthAction): AuthState {

  return {...state, loggedUser: action.payload};
}

function userLoggedOut(state: AuthState): AuthState {

  return {...state, loggedUser: null};
}
