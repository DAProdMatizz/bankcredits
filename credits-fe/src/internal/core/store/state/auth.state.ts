import {UserDto} from "../../../shared/dto/authentication/user.dto";

export interface AuthState {

  loggedUser: UserDto;
}
