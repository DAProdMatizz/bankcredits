import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Navigation} from "../../navigation";
import {AuthDto} from "../../dto/authentication/auth.dto";
import {UserDto} from "../../dto/authentication/user.dto";

@Injectable({
  providedIn: 'root'
})
export class AuthControllerService {

  constructor(private http: HttpClient) {
  }

  authenticate(authDto: AuthDto): Observable<UserDto> {

    return this.http.post<UserDto>(Navigation.authenticateUser, authDto);
  }
}
