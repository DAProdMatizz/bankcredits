import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ClientDto} from "../../dto/client/client.dto";
import {Navigation} from "../../navigation";
import {StringWrapperDto} from "../../dto/common/string-wrapper.dto";

@Injectable({
  providedIn: 'root'
})
export class ClientControllerService {

  constructor(private http: HttpClient) {
  }

  loadAllClients(): Observable<ClientDto[]> {

    return this.http.post<ClientDto[]>(Navigation.loadAllClients, null);
  }

  loadClientByEmail(wrapper: StringWrapperDto): Observable<ClientDto> {

    return this.http.post<ClientDto>(Navigation.loadClient, wrapper);
  }
}
