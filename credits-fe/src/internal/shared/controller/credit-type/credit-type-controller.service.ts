import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CreditTypeDto} from "../../dto/credit-type/credit-type.dto";
import {Navigation} from "../../navigation";

@Injectable({
  providedIn: 'root'
})
export class CreditTypeControllerService {

  constructor(private http: HttpClient) {
  }

  loadAllCreditTypes(): Observable<CreditTypeDto[]> {

    return this.http.post<CreditTypeDto[]>(Navigation.loadAllCreditTypes, null);
  }
}
