import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CreditDto} from "../../dto/credit/credit.dto";
import {Navigation} from "../../navigation";
import {CreditRequestDto} from "../../dto/credit/credit-request.dto";

@Injectable({
  providedIn: 'root'
})
export class CreditControllerService {

  constructor(private http: HttpClient) {
  }

  loadAllCredits(): Observable<CreditDto[]> {

    return this.http.post<CreditDto[]>(Navigation.loadAllCredits, null);
  }

  loadCreditById(creditId: number): Observable<CreditDto> {

    return this.http.post<CreditDto>(Navigation.loadCreditById(creditId), null);
  }

  loadClientCredits(clientId: number): Observable<CreditDto[]> {

    return this.http.post<CreditDto[]>(Navigation.loadClientCredits(clientId), null);
  }

  createCredit(request: CreditRequestDto): Observable<number> {

    return this.http.post<number>(Navigation.createCredit, request);
  }
}
