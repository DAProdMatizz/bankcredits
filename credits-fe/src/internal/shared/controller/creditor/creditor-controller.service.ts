import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CreditorDto} from "../../dto/creditor/creditor.dto";
import {Navigation} from "../../navigation";

@Injectable({
  providedIn: 'root'
})
export class CreditorControllerService {

  constructor(private http: HttpClient) {
  }

  loadAllCreditors(): Observable<CreditorDto[]> {

    return this.http.post<CreditorDto[]>(Navigation.loadAllCreditors, null);
  }
}
