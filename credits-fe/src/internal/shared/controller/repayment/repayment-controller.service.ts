import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RepaymentRequestDto} from "../../dto/repayment/repayment-request.dto";
import {Observable} from "rxjs";
import {Navigation} from "../../navigation";

@Injectable({
  providedIn: 'root'
})
export class RepaymentControllerService {

  constructor(private http: HttpClient) {

  }

  addPayment(paymentRequest: RepaymentRequestDto): Observable<number> {

    return this.http.post<number>(Navigation.addPayment, paymentRequest);
  }
}
