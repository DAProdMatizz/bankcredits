export interface UserDto {

  token: string;
  username: string;
  role: string;
  identity: string;
}
