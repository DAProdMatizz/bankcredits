export interface ClientDto {

  id: number;
  firstName: string;
  secondName: string;
  email: string;
}
