export interface CreditTypeDto {

  id: number;
  name: string;
  rate: number;
}
