import {ClientDto} from "../client/client.dto";

export interface CreditRequestDto {

  client?: ClientDto;
  creditTypeId: number;
  creditorId: number;
  sum: number;
  monthsAmount: number;
}
