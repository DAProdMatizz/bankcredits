import {ClientDto} from "../client/client.dto";
import {CreditTypeDto} from "../credit-type/credit-type.dto";
import {CreditorDto} from "../creditor/creditor.dto";
import {RepaymentDto} from "../repayment/repayment.dto";

export interface CreditDto {

  id: number;
  client: ClientDto;
  creditType: CreditTypeDto;
  creditor: CreditorDto;
  creditNumber: number;
  payedOff: boolean;
  openingDate: string;
  fullSum: number;
  monthsAmount: number;
  repayments: RepaymentDto[];
  repaymentSum?: number;
}
