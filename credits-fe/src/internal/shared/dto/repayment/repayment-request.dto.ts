import {CreditDto} from "../credit/credit.dto";

export interface RepaymentRequestDto {

  paymentSum: number;
  credit?: CreditDto;
}
