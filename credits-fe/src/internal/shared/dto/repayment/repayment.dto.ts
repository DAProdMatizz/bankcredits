import {CreditDto} from "../credit/credit.dto";

export interface RepaymentDto {

  id: number;
  credit: CreditDto;
  repaymentDate: string;
  repaymentSum: number;
}
