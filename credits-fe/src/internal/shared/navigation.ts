import {environment} from "../../environments/environment";

export class Navigation {

  private static readonly authenticate = '/authenticate';

  private static readonly clients = "/clients";
  private static readonly credits = "/credits";
  private static readonly creditors = "/creditors";
  private static readonly creditTypes = "/credit-types";
  private static readonly repayments = "/repayments";

  private static readonly client = "/client";

  private static readonly all = "/all";

  private static readonly load = "/load";
  private static readonly add = "/add";
  private static readonly update = "/update";
  private static readonly remove = "/delete";

  private static readonly loadAll = Navigation.load + Navigation.all;

  public static readonly loadClient = environment.baseUrl + Navigation.clients + Navigation.load;

  public static readonly loadAllClients = environment.baseUrl + Navigation.clients + Navigation.loadAll;
  public static readonly loadAllCredits = environment.baseUrl + Navigation.credits + Navigation.loadAll;
  public static readonly loadAllCreditors = environment.baseUrl + Navigation.creditors + Navigation.loadAll;
  public static readonly loadAllCreditTypes = environment.baseUrl + Navigation.creditTypes + Navigation.loadAll;
  public static readonly loadAllRepayments = environment.baseUrl + Navigation.repayments + Navigation.loadAll;

  public static readonly createCredit = environment.baseUrl + Navigation.credits + Navigation.add;

  public static readonly addPayment = environment.baseUrl + Navigation.repayments + Navigation.add;

  public static readonly authenticateUser = environment.baseUrl + Navigation.authenticate;

  public static loadClientCredits(clientId: number): string {

    return environment.baseUrl + Navigation.credits + Navigation.client + Navigation.loadById(clientId);
  }

  public static loadCreditById(creditId: number): string {

    return environment.baseUrl + Navigation.credits + Navigation.loadById(creditId);
  }

  private static loadById(id: number): string {

    return Navigation.load + Navigation.idPathVariable(id);
  }

  private static deleteById(id: number): string {

    return Navigation.remove + Navigation.idPathVariable(id);
  }

  private static idPathVariable(id: number): string {

    return '/' + String(id);
  }
}
