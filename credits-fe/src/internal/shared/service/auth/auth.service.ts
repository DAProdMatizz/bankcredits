import {Injectable} from '@angular/core';
import {AuthControllerService} from "../../controller/auth/auth-controller.service";
import {AuthDto} from "../../dto/authentication/auth.dto";
import {Observable, throwError} from "rxjs";
import {UserDto} from "../../dto/authentication/user.dto";
import {catchError, tap} from "rxjs/operators";
import {Router} from "@angular/router";
import {RouterUtils} from "../../utils/router-utils";
import {Store} from '@ngrx/store';
import {AppState} from "../../../core/store/state/app.state";
import {LoginAction} from "../../../core/store/action/login.action";
import {LogoutAction} from "../../../core/store/action/logout.action";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly AUTHENTICATED_USER_KEY = 'authenticatedUser';
  private readonly DEFAULT_ERROR_MESSAGE = 'Unknown error has occurred, please try again.';

  constructor(private authController: AuthControllerService, private router: Router, private store: Store<AppState>) {

  }

  login(authentication: AuthDto): Observable<UserDto> {

    return this.authController.authenticate(authentication).pipe(tap(user => {
      this.store.dispatch(new LoginAction(user));

      localStorage.setItem(this.AUTHENTICATED_USER_KEY, JSON.stringify(user));
    }), catchError(error => {
      if (!error.error) {
        return throwError(this.DEFAULT_ERROR_MESSAGE);
      }
      return throwError(error.error.message);
    }));
  }

  autoLogin(): void {

    const user: UserDto = JSON.parse(localStorage.getItem(this.AUTHENTICATED_USER_KEY)) as UserDto;
    if (user) {
      if (user.token) {
        this.store.dispatch(new LoginAction(user));
      }
    }
  }

  logout(): void {

    this.store.dispatch(new LogoutAction());

    this.router.navigate([RouterUtils.LOGIN]).then();
    localStorage.removeItem(this.AUTHENTICATED_USER_KEY);
  }
}
