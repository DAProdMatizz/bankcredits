import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ClientDto} from "../../dto/client/client.dto";
import {ClientControllerService} from "../../controller/client/client-controller.service";
import {StringWrapperDto} from "../../dto/common/string-wrapper.dto";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private clientController: ClientControllerService) {
  }

  loadClients(): Observable<ClientDto[]> {

    return this.clientController.loadAllClients();
  }

  loadClient(wrapper: StringWrapperDto): Observable<ClientDto> {

    return this.clientController.loadClientByEmail(wrapper);
  }
}
