import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {CreditTypeDto} from "../../dto/credit-type/credit-type.dto";
import {CreditTypeControllerService} from "../../controller/credit-type/credit-type-controller.service";

@Injectable({
  providedIn: 'root'
})
export class CreditTypeService {

  constructor(private creditTypeController: CreditTypeControllerService) {
  }

  loadCreditTypes(): Observable<CreditTypeDto[]> {

    return this.creditTypeController.loadAllCreditTypes();
  }
}
