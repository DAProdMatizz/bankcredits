import {Injectable} from '@angular/core';
import {Observable, Subject, throwError} from "rxjs";
import {CreditDto} from "../../dto/credit/credit.dto";
import {CreditControllerService} from "../../controller/credit/credit-controller.service";
import {catchError, exhaustMap, map, take, tap} from "rxjs/operators";
import {ClientService} from "../client/client.service";
import {Store} from "@ngrx/store";
import {AppState} from "../../../core/store/state/app.state";
import {StateUtils} from "../../../core/store/state/state.utils";
import {CreditRequestDto} from "../../dto/credit/credit-request.dto";
import {CreditUtils} from "../../utils/credit-utils";

@Injectable({
  providedIn: 'root'
})
export class CreditService {

  creditInitialized = new Subject<CreditDto>();

  constructor(private creditController: CreditControllerService, private clientService: ClientService, private store: Store<AppState>) {
  }

  loadCredits(): Observable<CreditDto[]> {

    return this.creditController.loadAllCredits();
  }

  loadCreditById(creditId): Observable<CreditDto> {

    return this.creditController.loadCreditById(creditId).pipe(tap(credit => {
      this.creditInitialized.next(credit);
    }));
  }

  loadClientCredits(): Observable<CreditDto[]> {

    return this.store.select(StateUtils.AUTH).pipe(
      take(1),
      map(authState => {
        return authState.loggedUser;
      }),
      exhaustMap(user => {
        return this.clientService.loadClient({value: user.username});
      }),
      exhaustMap(client => {
        return this.creditController.loadClientCredits(client.id);
      })
    );
  }

  createCredit(request: CreditRequestDto): Observable<number> {

    return this.store.select(StateUtils.AUTH).pipe(
      take(1),
      map(authState => {
        return authState.loggedUser;
      }),
      exhaustMap(user => {
        return this.clientService.loadClient({value: user.username});
      }),
      exhaustMap(client => {
        const creditRequest = CreditUtils.createCreditRequestWithClient(request, client);
        return this.creditController.createCredit(creditRequest);
      }),
      catchError(error => {
        return throwError(error.error.message)
      })
    );
  }
}
