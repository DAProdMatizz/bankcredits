import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {CreditorDto} from "../../dto/creditor/creditor.dto";
import {CreditorControllerService} from "../../controller/creditor/creditor-controller.service";

@Injectable({
  providedIn: 'root'
})
export class CreditorService {

  constructor(private creditorController: CreditorControllerService) {
  }

  loadCreditors(): Observable<CreditorDto[]> {

    return this.creditorController.loadAllCreditors();
  }
}
