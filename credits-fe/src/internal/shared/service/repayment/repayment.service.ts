import {Injectable} from '@angular/core';
import {RepaymentControllerService} from "../../controller/repayment/repayment-controller.service";
import {RepaymentRequestDto} from "../../dto/repayment/repayment-request.dto";
import {CreditDto} from "../../dto/credit/credit.dto";
import {Observable, throwError} from "rxjs";
import {CreditUtils} from "../../utils/credit-utils";
import {catchError} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root'
})
export class RepaymentService {

  constructor(private repaymentController: RepaymentControllerService) {

  }

  addPayment(formValue: RepaymentRequestDto, credit: CreditDto): Observable<number> {

    const paymentRequest = CreditUtils.createRepaymentRequestWithCredit(formValue, credit);
    return this.repaymentController.addPayment(paymentRequest).pipe(catchError(error => {
      return throwError(error.error.message)
    }));
  }
}
