import {RouterUtils} from "./router-utils";
import {HttpHeaders} from "@angular/common/http";

export class AuthUtils {

  private static readonly AUTHORIZATION_HEADER_KEY = 'Authorization';
  private static readonly BEARER_PREFIX = 'Bearer ';

  public static readonly ADMIN_ROLE = 'admin';
  public static readonly CLIENT_ROLE = 'client';

  public static readonly HTTP_STATUS_UNAUTHORIZED = 401;

  static navigationPathByRole(role: string): string {

    if (role === AuthUtils.ADMIN_ROLE) {
      return RouterUtils.ADMIN + RouterUtils.CREDITS;
    } else if (role === AuthUtils.CLIENT_ROLE) {
      return RouterUtils.USER + RouterUtils.CREDITS;
    }
  }

  static buildAuthorizationHeader(token: string): HttpHeaders {

    const bearerToken = this.BEARER_PREFIX + token;
    return new HttpHeaders({[this.AUTHORIZATION_HEADER_KEY]: bearerToken});
  }
}
