import {CreditDto} from "../dto/credit/credit.dto";
import {RepaymentDto} from "../dto/repayment/repayment.dto";
import {CreditRequestDto} from "../dto/credit/credit-request.dto";
import {ClientDto} from "../dto/client/client.dto";
import {RepaymentRequestDto} from "../dto/repayment/repayment-request.dto";

export class CreditUtils {

  public static readonly TEN = 10;
  public static readonly ONE_HUNDRED = 100;

  public static readonly DEFAULT_PRECISION = 1;

  public static readonly PAYED = "Payed Off";
  public static readonly IN_PROCESS = "Payment in process";

  public static readonly LEFT_TO_PAY = "Left to pay";

  public static calculateRepaymentSumInPercent(credit: CreditDto): number {

    const fullSum: number = credit.fullSum;
    const repaymentSum: number = CreditUtils.calculateCreditRepaymentSum(credit);

    const notRoundedPercent = repaymentSum / fullSum * CreditUtils.ONE_HUNDRED;
    return this.precisionRound(notRoundedPercent);
  }

  public static calculatePaymentPercent(repaymentSum: number, creditFullSum: number): number {

    const notRoundedPercent = repaymentSum / creditFullSum * CreditUtils.ONE_HUNDRED;
    return this.precisionRound(notRoundedPercent);
  }

  public static calculateCreditRepaymentSum(credit: CreditDto): number {

    if (credit.repayments === null) {
      return 0;
    }

    const repayments = credit.repayments.map((repayment: RepaymentDto) => {
      return repayment.repaymentSum;
    });

    return repayments.reduce((prev: number, current: number) => prev + current, 0);
  }

  public static createEmptyCreditDto(): CreditDto {

    return {
      client: {
        id: null,
        firstName: null,
        secondName: null,
        email: null
      },
      creditNumber: null,
      creditType: {
        id: null,
        name: null,
        rate: null
      },
      creditor: {
        id: null,
        name: null
      },
      fullSum: null,
      id: null,
      openingDate: null,
      payedOff: false,
      repaymentSum: null,
      monthsAmount: null,
      repayments: []
    };
  }

  public static createCreditRequestWithClient(initialRequest: CreditRequestDto, client: ClientDto): CreditRequestDto {

    return {
      client: client,
      creditorId: initialRequest.creditorId,
      creditTypeId: initialRequest.creditTypeId,
      monthsAmount: Number(initialRequest.monthsAmount),
      sum: Number(initialRequest.sum)
    };
  }

  public static createRepaymentRequestWithCredit(initialRequest: RepaymentRequestDto, credit: CreditDto): RepaymentRequestDto {

    return {
      paymentSum: Number(initialRequest.paymentSum),
      credit: credit
    };
  }

  private static precisionRound(number: number): number {

    return Number(Math.round(Number(number + "e+" + CreditUtils.DEFAULT_PRECISION)) + "e-" + CreditUtils.DEFAULT_PRECISION);
  }
}
