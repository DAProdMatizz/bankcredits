export class RouterUtils {

  public static readonly ONE_LEVEL_BACK = '../';
  public static readonly LOGIN = '/login';
  public static readonly USER = '/user';
  public static readonly ADMIN = '/admin';
  public static readonly CREDITS = '/credits';
}
