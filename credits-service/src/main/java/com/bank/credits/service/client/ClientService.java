package com.bank.credits.service.client;

import com.bank.credits.dto.client.ClientDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ClientService {

	List<ClientDto> loadAll();

	ClientDto loadById(long id);

	ClientDto loadByEmail(String email);

	long add(ClientDto client);

	void update(ClientDto client);

	void delete(long id);
}
