package com.bank.credits.service.client.impl;

import com.bank.credits.dto.client.ClientDto;
import com.bank.credits.orm.entity.ClientEntity;
import com.bank.credits.orm.repository.ClientRepository;
import com.bank.credits.service.client.ClientService;
import com.bank.credits.service.mapper.DtoMapper;
import com.bank.credits.service.mapper.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

	private final ClientRepository clientRepository;

	@Autowired
	public ClientServiceImpl(ClientRepository clientRepository) {

		this.clientRepository = clientRepository;
	}

	@Override
	public List<ClientDto> loadAll() {

		var entities = clientRepository.findAll();
		return entities.stream().map(DtoMapper::toClientDto).collect(Collectors.toList());
	}

	@Override
	public ClientDto loadById(long id) {

		ClientEntity entity = clientRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		return DtoMapper.toClientDto(entity);
	}

	@Override
	public ClientDto loadByEmail(String email) {

		ClientEntity entity = clientRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
		return DtoMapper.toClientDto(entity);
	}

	@Override
	public long add(ClientDto client) {

		ClientEntity clientEntity = EntityMapper.toClientEntity(client.getFirstName(), client.getSecondName(), client.getEmail());
		ClientEntity entity = clientRepository.save(clientEntity);
		return entity.getId();
	}

	@Override
	public void update(ClientDto client) {

		ClientEntity entity = clientRepository.findById(client.getId()).orElseThrow(EntityNotFoundException::new);
		entity.setFirstName(client.getFirstName());
		entity.setSecondName(client.getSecondName());
		entity.setEmail(client.getEmail());
		clientRepository.save(entity);
	}

	@Override
	public void delete(long id) {

		ClientEntity entity = clientRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		clientRepository.delete(entity);
	}
}
