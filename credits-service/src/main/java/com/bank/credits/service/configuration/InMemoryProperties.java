package com.bank.credits.service.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "in-memory")
public class InMemoryProperties {

	private List<User> users = new ArrayList<>();

	public List<User> getUsers() {

		return users;
	}

	public void setUsers(List<User> users) {

		this.users = users;
	}

	public static class User {

		private String username;

		private String password;

		private String role;

		public String getUsername() {

			return username;
		}

		public void setUsername(String username) {

			this.username = username;
		}

		public String getPassword() {

			return password;
		}

		public void setPassword(String password) {

			this.password = password;
		}

		public String getRole() {

			return role;
		}

		public void setRole(String role) {

			this.role = role;
		}
	}
}
