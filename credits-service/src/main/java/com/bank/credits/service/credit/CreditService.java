package com.bank.credits.service.credit;

import com.bank.credits.dto.credit.CreditDto;
import com.bank.credits.dto.credit.CreditRequestDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CreditService {

	List<CreditDto> loadAll();

	CreditDto loadById(Long id);

	List<CreditDto> loadClientCredits(Long clientId);

	long createCredit(CreditRequestDto creditRequest);
}
