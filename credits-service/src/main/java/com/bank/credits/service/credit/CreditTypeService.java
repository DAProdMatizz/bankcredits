package com.bank.credits.service.credit;

import com.bank.credits.dto.credit.CreditTypeDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CreditTypeService {

	List<CreditTypeDto> loadAll();

	CreditTypeDto loadById(long id);

	long add(CreditTypeDto creditType);

	void update(CreditTypeDto creditType);

	void delete(long id);
}
