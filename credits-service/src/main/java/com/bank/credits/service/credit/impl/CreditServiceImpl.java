package com.bank.credits.service.credit.impl;

import com.bank.credits.common.utils.CollectionUtils;
import com.bank.credits.common.utils.DateUtils;
import com.bank.credits.dto.credit.CreditDto;
import com.bank.credits.dto.credit.CreditRequestDto;
import com.bank.credits.orm.entity.ClientEntity;
import com.bank.credits.orm.entity.CreditEntity;
import com.bank.credits.orm.entity.CreditTypeEntity;
import com.bank.credits.orm.entity.CreditorEntity;
import com.bank.credits.orm.repository.*;
import com.bank.credits.service.credit.CreditService;
import com.bank.credits.service.exception.IncorrectMonthsAmountException;
import com.bank.credits.service.exception.IncorrectSumException;
import com.bank.credits.service.exception.ObjectNullException;
import com.bank.credits.service.mapper.DtoMapper;
import com.bank.credits.service.mapper.EntityMapper;
import com.bank.credits.service.utils.CreditUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CreditServiceImpl implements CreditService {

	private final CreditRepository creditRepository;
	private final RepaymentRepository repaymentRepository;
	private final CreditTypeRepository creditTypeRepository;
	private final CreditorRepository creditorRepository;
	private final ClientRepository clientRepository;

	@Autowired
	public CreditServiceImpl(CreditRepository creditRepository, RepaymentRepository repaymentRepository, CreditTypeRepository creditTypeRepository,
			CreditorRepository creditorRepository, ClientRepository clientRepository) {

		this.creditRepository = creditRepository;
		this.repaymentRepository = repaymentRepository;
		this.creditTypeRepository = creditTypeRepository;
		this.creditorRepository = creditorRepository;
		this.clientRepository = clientRepository;
	}

	@Override
	public List<CreditDto> loadAll() {

		var creditEntities = creditRepository.findAll();
		return creditEntities.stream().map(this::buildCreditDto).collect(Collectors.toList());
	}

	@Override
	public CreditDto loadById(Long id) {

		CreditEntity creditEntity = this.creditRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		return buildCreditDto(creditEntity);
	}

	@Override
	public List<CreditDto> loadClientCredits(Long clientId) {

		var clientCredits = creditRepository.findAllByClientId(clientId);
		return clientCredits.stream().map(this::buildCreditDto).collect(Collectors.toList());
	}

	@Override
	public long createCredit(CreditRequestDto creditRequest) {

		validate(creditRequest);

		Long creditorId = creditRequest.getCreditorId();
		Long clientId = creditRequest.getClient().getId();
		BigDecimal creditSum = creditRequest.getSum();
		BigDecimal creditMonthsAmount = creditRequest.getMonthsAmount();

		CreditTypeEntity creditType = getCreditType(creditRequest);
		CreditorEntity creditor = getCreditor(creditorId);
		ClientEntity client = getClient(clientId);

		BigDecimal creditRate = creditType.getRate();

		BigDecimal creditNumber = calculateCreditNumber(creditType.getId(), creditorId, clientId);
		BigDecimal creditFullSum = CreditUtils.calculateCreditFullSum(creditSum, creditMonthsAmount, creditRate);

		CreditEntity creditEntity = EntityMapper.toCreditEntity(client, creditType, creditor, creditNumber, DateUtils.now(), creditFullSum, creditMonthsAmount);

		CreditEntity entity = creditRepository.save(creditEntity);
		return entity.getId();
	}

	private CreditDto buildCreditDto(CreditEntity creditEntity) {

		var creditRepayments = repaymentRepository.findAllByCreditId(creditEntity.getId());

		if (CollectionUtils.nullOrEmpty(creditRepayments)) {
			return DtoMapper.toCreditDto(creditEntity);
		}

		return DtoMapper.toCreditDto(creditEntity, creditRepayments);
	}

	private BigDecimal calculateCreditNumber(Long creditTypeId, Long creditorId, Long clientId) {

		BigDecimal creditTypeBigDecimal = new BigDecimal(creditTypeId);
		BigDecimal creditorBigDecimal = new BigDecimal(creditorId);
		BigDecimal clientBigDecimal = new BigDecimal(clientId);

		return CreditUtils.calculateCreditNumber(creditTypeBigDecimal, creditorBigDecimal, clientBigDecimal);
	}

	private CreditTypeEntity getCreditType(CreditRequestDto creditRequest) {

		Long creditTypeId = creditRequest.getCreditTypeId();
		BigDecimal creditSum = creditRequest.getSum();
		BigDecimal creditMonthsAmount = creditRequest.getMonthsAmount();

		CreditTypeEntity creditType = creditTypeRepository.findById(creditTypeId).orElseThrow(EntityNotFoundException::new);

		Integer minSum = creditType.getMinSum();
		Integer minMonths = creditType.getMinMonths();
		String creditTypeName = creditType.getName();

		checkCreditSum(creditSum, new BigDecimal(minSum), creditTypeName);
		checkCreditMonthsAmount(creditMonthsAmount, new BigDecimal(minMonths), creditTypeName);

		return creditType;
	}

	private CreditorEntity getCreditor(Long creditorId) {

		return creditorRepository.findById(creditorId).orElseThrow(EntityNotFoundException::new);
	}

	private ClientEntity getClient(Long clientId) {

		return clientRepository.findById(clientId).orElseThrow(EntityNotFoundException::new);
	}

	private void checkCreditSum(BigDecimal sum, BigDecimal minSum, String creditTypeName) {

		if (sum.compareTo(minSum) < 0) {
			throw new IncorrectSumException(sum, minSum, creditTypeName);
		}
	}

	private void checkCreditMonthsAmount(BigDecimal monthsAmount, BigDecimal minMonths, String creditTypeName) {

		if (monthsAmount.compareTo(minMonths) < 0) {
			throw new IncorrectMonthsAmountException(monthsAmount, minMonths, creditTypeName);
		}
	}

	private void validate(CreditRequestDto dto) {

		if (Objects.isNull(dto)) {
			throw new ObjectNullException("credit");
		}

		if (Objects.isNull(dto.getClient())) {
			throw new ObjectNullException("credit.client");
		}

		if (Objects.isNull(dto.getClient().getId())) {
			throw new ObjectNullException("credit.client.id");
		}

		if (Objects.isNull(dto.getCreditTypeId())) {
			throw new ObjectNullException("credit.creditTypeId");
		}

		if (Objects.isNull(dto.getCreditorId())) {
			throw new ObjectNullException("credit.creditorId");
		}

		BigDecimal sum = dto.getSum();

		if (Objects.isNull(sum)) {
			throw new IncorrectSumException("Required property [credit.sum] is null");
		}

		if (sum.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IncorrectSumException(sum);
		}

		BigDecimal monthsAmount = dto.getMonthsAmount();

		if (Objects.isNull(monthsAmount)) {
			throw new IncorrectMonthsAmountException("Required property [credit.monthsAmount] is null");
		}

		if (monthsAmount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IncorrectMonthsAmountException(monthsAmount);
		}
	}
}
