package com.bank.credits.service.credit.impl;

import com.bank.credits.dto.credit.CreditTypeDto;
import com.bank.credits.orm.entity.CreditTypeEntity;
import com.bank.credits.orm.repository.CreditTypeRepository;
import com.bank.credits.service.credit.CreditTypeService;
import com.bank.credits.service.mapper.DtoMapper;
import com.bank.credits.service.mapper.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CreditTypeServiceImpl implements CreditTypeService {

	private final CreditTypeRepository creditTypeRepository;

	@Autowired
	public CreditTypeServiceImpl(CreditTypeRepository creditTypeRepository) {

		this.creditTypeRepository = creditTypeRepository;
	}

	@Override
	public List<CreditTypeDto> loadAll() {

		var entities = creditTypeRepository.findAll();
		return entities.stream().map(DtoMapper::toCreditTypeDto).collect(Collectors.toList());
	}

	@Override
	public CreditTypeDto loadById(long id) {

		CreditTypeEntity entity = creditTypeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		return DtoMapper.toCreditTypeDto(entity);
	}

	@Override
	public long add(CreditTypeDto creditType) {

		CreditTypeEntity creditTypeEntity = EntityMapper.toCreditTypeEntity(creditType.getName(), creditType.getRate(), creditType.getMinSum(), creditType.getMinMonths());
		CreditTypeEntity entity = creditTypeRepository.save(creditTypeEntity);
		return entity.getId();
	}

	@Override
	public void update(CreditTypeDto creditType) {

		CreditTypeEntity entity = creditTypeRepository.findById(creditType.getId()).orElseThrow(EntityNotFoundException::new);
		entity.setName(creditType.getName());
		entity.setRate(creditType.getRate());
		creditTypeRepository.save(entity);
	}

	@Override
	public void delete(long id) {

		CreditTypeEntity entity = creditTypeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		creditTypeRepository.delete(entity);
	}
}
