package com.bank.credits.service.creditor;

import com.bank.credits.dto.creditor.CreditorDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CreditorService {

	List<CreditorDto> loadAll();

	CreditorDto loadById(long id);

	long add(CreditorDto creditor);

	void update(CreditorDto creditor);

	void delete(long id);
}
