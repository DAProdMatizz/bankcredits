package com.bank.credits.service.creditor.impl;

import com.bank.credits.dto.creditor.CreditorDto;
import com.bank.credits.orm.entity.CreditorEntity;
import com.bank.credits.orm.repository.CreditorRepository;
import com.bank.credits.service.creditor.CreditorService;
import com.bank.credits.service.mapper.DtoMapper;
import com.bank.credits.service.mapper.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CreditorServiceImpl implements CreditorService {

	private final CreditorRepository creditorRepository;

	@Autowired
	public CreditorServiceImpl(CreditorRepository creditorRepository) {

		this.creditorRepository = creditorRepository;
	}

	@Override
	public List<CreditorDto> loadAll() {

		var entities = creditorRepository.findAll();
		return entities.stream().map(DtoMapper::toCreditorDto).collect(Collectors.toList());
	}

	@Override
	public CreditorDto loadById(long id) {

		CreditorEntity entity = creditorRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		return DtoMapper.toCreditorDto(entity);
	}

	@Override
	public long add(CreditorDto creditor) {

		CreditorEntity creditorEntity = EntityMapper.toCreditorEntity(creditor.getName());
		CreditorEntity entity = creditorRepository.save(creditorEntity);
		return entity.getId();
	}

	@Override
	public void update(CreditorDto creditor) {

		CreditorEntity entity = creditorRepository.findById(creditor.getId()).orElseThrow(EntityNotFoundException::new);
		entity.setName(creditor.getName());
		creditorRepository.save(entity);
	}

	@Override
	public void delete(long id) {

		CreditorEntity entity = creditorRepository.findById(id).orElseThrow(EntityNotFoundException::new);
		creditorRepository.delete(entity);
	}
}
