package com.bank.credits.service.exception;

import com.bank.credits.common.exception.BankCreditsException;

public class BankServiceException extends BankCreditsException {

	public BankServiceException(String message) {

		super(message);
	}

	public BankServiceException(Throwable cause) {

		super(cause);
	}

	public BankServiceException(String message, Throwable cause) {

		super(message, cause);
	}
}
