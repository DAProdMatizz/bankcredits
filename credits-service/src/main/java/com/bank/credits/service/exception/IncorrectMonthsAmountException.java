package com.bank.credits.service.exception;

import java.math.BigDecimal;

public class IncorrectMonthsAmountException extends BankServiceException {

	public IncorrectMonthsAmountException(BigDecimal monthsAmount) {

		super("Months amount is equal or less than zero [" + monthsAmount + "]");
	}

	public IncorrectMonthsAmountException(BigDecimal monthsAmount, BigDecimal minMonths, String creditTypeName) {

		super("Months amount [" + monthsAmount + "] is lesser than minimum for the credit type ["+ creditTypeName + " (" + minMonths + ")]");
	}

	public IncorrectMonthsAmountException(String message) {

		super(message);
	}
}
