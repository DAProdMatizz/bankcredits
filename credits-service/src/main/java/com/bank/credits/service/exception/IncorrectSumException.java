package com.bank.credits.service.exception;

import java.math.BigDecimal;

public class IncorrectSumException extends BankServiceException {

	public IncorrectSumException(BigDecimal sum) {

		super("Sum is equal or less than zero [" + sum + "]");
	}

	public IncorrectSumException(BigDecimal sum, BigDecimal minSum, String creditTypeName) {

		super("Sum of credit [" + sum + "] is lesser than minimum for the credit type ["+ creditTypeName + " (" + minSum + ")]");
	}

	public IncorrectSumException(BigDecimal paymentSum, BigDecimal minSum) {

		super("Payment sum [" + paymentSum + "] is lesser than minimum for the credit [" + minSum + "]");
	}

	public IncorrectSumException(String message) {

		super(message);
	}
}
