package com.bank.credits.service.exception;

public class ObjectNullException extends BankServiceException {

	public ObjectNullException(String message) {

		super("The object " + message + " is null");
	}
}
