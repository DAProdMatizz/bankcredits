package com.bank.credits.service.exception;

public class UserNotFoundException extends BankServiceException {

	public UserNotFoundException(String username) {

		super("Username [" + username + "] not found");
	}

	public UserNotFoundException(long id) {

		super("User with id [" + id + "] not found");
	}
}
