package com.bank.credits.service.mapper;

import com.bank.credits.common.exception.NonInstanceException;
import com.bank.credits.common.utils.CollectionUtils;
import com.bank.credits.dto.client.ClientDto;
import com.bank.credits.dto.client.builder.ClientDtoBuilder;
import com.bank.credits.dto.credit.CreditDto;
import com.bank.credits.dto.credit.CreditTypeDto;
import com.bank.credits.dto.credit.builder.CreditDtoBuilder;
import com.bank.credits.dto.credit.builder.CreditTypeDtoBuilder;
import com.bank.credits.dto.creditor.CreditorDto;
import com.bank.credits.dto.creditor.builder.CreditorDtoBuilder;
import com.bank.credits.dto.repayment.RepaymentDto;
import com.bank.credits.dto.repayment.builder.RepaymentDtoBuilder;
import com.bank.credits.orm.entity.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DtoMapper {

	private DtoMapper() {

		throw new NonInstanceException(DtoMapper.class);
	}

	public static ClientDto toClientDto(ClientEntity entity) {

		if (Objects.isNull(entity)) {
			return null;
		}

		return ClientDtoBuilder.newBuilder()
				.id(entity.getId())
				.firstName(entity.getFirstName())
				.secondName(entity.getSecondName())
				.email(entity.getEmail())
				.build();
	}

	public static CreditTypeDto toCreditTypeDto(CreditTypeEntity entity) {

		if (Objects.isNull(entity)) {
			return null;
		}

		return CreditTypeDtoBuilder.newBuilder()
				.id(entity.getId())
				.name(entity.getName())
				.rate(entity.getRate())
				.minSum(entity.getMinSum())
				.minMonths(entity.getMinMonths())
				.build();
	}

	public static CreditorDto toCreditorDto(CreditorEntity entity) {

		if (Objects.isNull(entity)) {
			return null;
		}

		return CreditorDtoBuilder.newBuilder()
				.id(entity.getId())
				.name(entity.getName())
				.build();
	}

	public static CreditDto toCreditDto(CreditEntity creditEntity) {

		if (Objects.isNull(creditEntity)) {
			return null;
		}

		return CreditDtoBuilder.newBuilder()
				.id(creditEntity.getId())
				.client(toClientDto(creditEntity.getClient()))
				.creditType(toCreditTypeDto(creditEntity.getCreditType()))
				.creditor(toCreditorDto(creditEntity.getCreditor()))
				.number(creditEntity.getNumber())
				.payedOff(creditEntity.isPayedOff())
				.openingDate(creditEntity.getOpeningDate())
				.fullSum(creditEntity.getSum())
				.monthsAmount(creditEntity.getMonthsAmount())
				.build();
	}

	public static CreditDto toCreditDto(CreditEntity creditEntity, List<RepaymentEntity> repaymentEntities) {

		if (CollectionUtils.nullOrEmpty(repaymentEntities) || Objects.isNull(creditEntity)) {
			return null;
		}

		var repayments = repaymentEntities.stream().map(DtoMapper::toRepaymentDto).collect(Collectors.toList());

		return CreditDtoBuilder.newBuilder()
				.id(creditEntity.getId())
				.client(toClientDto(creditEntity.getClient()))
				.creditType(toCreditTypeDto(creditEntity.getCreditType()))
				.creditor(toCreditorDto(creditEntity.getCreditor()))
				.number(creditEntity.getNumber())
				.payedOff(creditEntity.isPayedOff())
				.openingDate(creditEntity.getOpeningDate())
				.fullSum(creditEntity.getSum())
				.monthsAmount(creditEntity.getMonthsAmount())
				.repayments(repayments)
				.build();
	}

	public static RepaymentDto toRepaymentDto(RepaymentEntity entity) {

		if (Objects.isNull(entity)) {
			return null;
		}

		return RepaymentDtoBuilder.newBuilder()
				.id(entity.getId())
				.repaymentDate(entity.getRepaymentDate())
				.repaymentSum(entity.getSum())
				.build();
	}
}
