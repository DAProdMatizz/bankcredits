package com.bank.credits.service.mapper;

import com.bank.credits.common.exception.NonInstanceException;
import com.bank.credits.orm.entity.*;

import java.math.BigDecimal;
import java.util.Date;

public final class EntityMapper {

	private EntityMapper() {

		throw new NonInstanceException(EntityMapper.class);
	}

	public static ClientEntity toClientEntity(String forename, String surname, String email) {

		return new ClientEntity(forename, surname, email);
	}

	public static CreditTypeEntity toCreditTypeEntity(String name, BigDecimal rate, Integer minSum, Integer minMonths) {

		return new CreditTypeEntity(name, rate, minSum, minMonths);
	}

	public static CreditorEntity toCreditorEntity(String name) {

		return new CreditorEntity(name);
	}

	public static CreditEntity toCreditEntity(ClientEntity client, CreditTypeEntity creditType, CreditorEntity creditor, BigDecimal number, Date openingDate, BigDecimal sum,
			BigDecimal monthsAmount) {

		return new CreditEntity(client, creditType, creditor, number, openingDate, sum, monthsAmount);
	}

	public static RepaymentEntity toRepaymentEntity(CreditEntity credit, Date repaymentDate, BigDecimal sum) {

		return new RepaymentEntity(credit, repaymentDate, sum);
	}
}
