package com.bank.credits.service.repayment;

import com.bank.credits.dto.repayment.RepaymentDto;
import com.bank.credits.dto.repayment.RepaymentRequestDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface RepaymentService {

	List<RepaymentDto> loadAll();

	long addPayment(RepaymentRequestDto request);
}
