package com.bank.credits.service.repayment.impl;

import com.bank.credits.common.utils.DateUtils;
import com.bank.credits.dto.repayment.RepaymentDto;
import com.bank.credits.dto.repayment.RepaymentRequestDto;
import com.bank.credits.orm.entity.CreditEntity;
import com.bank.credits.orm.entity.RepaymentEntity;
import com.bank.credits.orm.repository.CreditRepository;
import com.bank.credits.orm.repository.RepaymentRepository;
import com.bank.credits.service.exception.IncorrectSumException;
import com.bank.credits.service.exception.ObjectNullException;
import com.bank.credits.service.mapper.DtoMapper;
import com.bank.credits.service.mapper.EntityMapper;
import com.bank.credits.service.repayment.RepaymentService;
import com.bank.credits.service.utils.CreditUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RepaymentServiceImpl implements RepaymentService {

	private final RepaymentRepository repaymentRepository;
	private final CreditRepository creditRepository;

	@Autowired
	public RepaymentServiceImpl(RepaymentRepository repaymentRepository, CreditRepository creditRepository) {

		this.repaymentRepository = repaymentRepository;
		this.creditRepository = creditRepository;
	}

	@Override
	public List<RepaymentDto> loadAll() {

		var entities = repaymentRepository.findAll();
		return entities.stream().map(DtoMapper::toRepaymentDto).collect(Collectors.toList());
	}

	@Override
	public long addPayment(RepaymentRequestDto request) {

		validate(request);

		BigDecimal paymentSum = request.getPaymentSum();

		CreditEntity credit = getCredit(request);
		RepaymentEntity repaymentEntity = EntityMapper.toRepaymentEntity(credit, DateUtils.now(), paymentSum);

		RepaymentEntity entity = repaymentRepository.save(repaymentEntity);
		return entity.getId();
	}

	private CreditEntity getCredit(RepaymentRequestDto request) {

		Long creditId = request.getCredit().getId();
		BigDecimal paymentSum = request.getPaymentSum();
		BigDecimal fullSum = request.getCredit().getFullSum();
		BigDecimal monthsAmount = request.getCredit().getMonthsAmount();

		CreditEntity credit = creditRepository.findById(creditId).orElseThrow(EntityNotFoundException::new);

		checkPaymentSum(paymentSum, fullSum, monthsAmount);

		return credit;
	}

	private void checkPaymentSum(BigDecimal paymentSum, BigDecimal fullSum, BigDecimal monthsAmount) {

		BigDecimal minMonthlySum = CreditUtils.calculateMonthlySum(fullSum, monthsAmount);

		if (paymentSum.compareTo(minMonthlySum) < 0) {
			throw new IncorrectSumException(paymentSum, minMonthlySum);
		}
	}

	private void validate(RepaymentRequestDto request) {

		if (Objects.isNull(request)) {
			throw new ObjectNullException("payment");
		}

		BigDecimal paymentSum = request.getPaymentSum();

		if (Objects.isNull(paymentSum)) {
			throw new IncorrectSumException("Required property [payment.paymentSum] is null");
		}

		if (paymentSum.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IncorrectSumException(paymentSum);
		}

		if (Objects.isNull(request.getCredit())) {
			throw new ObjectNullException("payment.credit");
		}

		if (Objects.isNull(request.getCredit().getId())) {
			throw new ObjectNullException("payment.credit.id");
		}
	}
}
