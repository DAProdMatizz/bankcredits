package com.bank.credits.service.user;

import com.bank.credits.dto.user.UserDto;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserService {

	UserDto getUserByUsername(String username);
}
