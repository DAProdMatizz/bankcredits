package com.bank.credits.service.user.impl;

import com.bank.credits.dto.user.UserDto;
import com.bank.credits.service.configuration.InMemoryProperties;
import com.bank.credits.service.configuration.InMemoryProperties.User;
import com.bank.credits.service.exception.UserNotFoundException;
import com.bank.credits.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	private final InMemoryProperties properties;

	@Autowired
	public UserServiceImpl(InMemoryProperties properties) {

		this.properties = properties;
	}

	@Override
	public UserDto getUserByUsername(String username) {

		User user = properties.getUsers().stream().filter(element -> element.getUsername().equals(username)).findAny().orElseThrow(() -> new UserNotFoundException(username));
		return new UserDto(user.getUsername(), user.getPassword(), user.getRole());
	}
}
