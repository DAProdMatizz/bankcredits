package com.bank.credits.service.utils;

import com.bank.credits.common.exception.NonInstanceException;
import com.bank.credits.common.utils.DateUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

public final class CreditUtils {

	private static final int SCALE = 0;

	private static final BigDecimal HUNDRED = new BigDecimal(100);
	private static final BigDecimal TEN_THOUSAND = new BigDecimal(10000);
	private static final BigDecimal HUNDRED_MILLION = new BigDecimal(100000000);

	private CreditUtils() {

		throw new NonInstanceException(CreditUtils.class);
	}

	public static BigDecimal calculateCreditNumber(BigDecimal creditTypeId, BigDecimal creditorId, BigDecimal clientId) {

		BigDecimal idSum = creditTypeId.add(creditorId).add(clientId);
		BigDecimal idsPart = HUNDRED_MILLION.multiply(idSum);

		LocalDate now = DateUtils.toLocalDate(DateUtils.now());

		BigDecimal year = new BigDecimal(now.getYear());
		BigDecimal month = new BigDecimal(now.getMonth().getValue());
		BigDecimal dayOfMonth = new BigDecimal(now.getDayOfMonth());

		BigDecimal monthPart = HUNDRED.multiply(month);
		BigDecimal yearPart = TEN_THOUSAND.multiply(year);

		return idsPart.add(yearPart).add(monthPart).add(dayOfMonth);
	}

	public static BigDecimal calculateCreditFullSum(BigDecimal creditSum, BigDecimal creditMonthsAmount, BigDecimal creditRate) {

		BigDecimal monthSumWithoutRate = creditSum.divide(creditMonthsAmount, SCALE, RoundingMode.CEILING);
		BigDecimal rateMultiplier = BigDecimal.ONE.add(creditRate);
		BigDecimal monthSumWithRate = monthSumWithoutRate.multiply(rateMultiplier);
		return monthSumWithRate.multiply(creditMonthsAmount);
	}

	public static BigDecimal calculateMonthlySum(BigDecimal creditFullSum, BigDecimal creditMonthsAmount) {

		return creditFullSum.divide(creditMonthsAmount, SCALE, RoundingMode.CEILING);
	}
}
