package com.bank.credits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankCreditsApplication {

	public static void main(String[] args) {

		SpringApplication.run(BankCreditsApplication.class, args);
	}
}
