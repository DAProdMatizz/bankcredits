package com.bank.credits.web;

public interface Navigation {

	String ALL_PATTERN_EXT = "/**";

	String AUTHENTICATE = "/authenticate";

	String ACTUATOR = "/actuator";
	String ACTUATOR_ALL_PATTERN_EXT = ACTUATOR + ALL_PATTERN_EXT;

	String CLIENTS = "/clients";
	String CREDITS = "/credits";
	String CREDITORS = "/creditors";
	String CREDIT_TYPES = "/credit-types";
	String REPAYMENTS = "/repayments";

	String CLIENT = "/client";

	String ALL = "/all";

	String LOAD = "/load";
	String ADD = "/add";
	String UPDATE = "/update";
	String DELETE = "/delete";

	String LOAD_ALL = LOAD + ALL;

	String ID = "id";
	String ID_PATH_PARAM = "/{" + ID + "}";
	String LOAD_BY_ID = LOAD + ID_PATH_PARAM;
	String DELETE_BY_ID = DELETE + ID_PATH_PARAM;

	String LOAD_BY_CLIENT_ID = CLIENT + LOAD_BY_ID;
}
