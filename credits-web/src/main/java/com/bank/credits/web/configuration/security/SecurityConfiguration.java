package com.bank.credits.web.configuration.security;

import com.bank.credits.web.Navigation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private UnauthorizedEntryPoint unauthorizedEntryPoint;

	@Autowired
	private TokenComponent tokenComponent;

	@Autowired
	private TokenHolder tokenHolder;

	@Autowired
	private AuthenticationProvider authenticationProvider;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {

		builder.authenticationProvider(authenticationProvider);
	}

	@Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {

		return super.authenticationManager();
	}

	@Bean
	//TODO: improve
	public WebMvcConfigurer corsConfigurer() {

		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {

				registry
						.addMapping(Navigation.ALL_PATTERN_EXT)
						.allowedHeaders("*")
						.allowCredentials(true)
						.allowedOrigins("http://localhost:4200");
			}
		};
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
				.csrf().disable()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
					.exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint)
				.and()
					.authorizeRequests()
						.antMatchers(HttpMethod.OPTIONS).permitAll()
						.antMatchers(HttpMethod.GET).permitAll()
						.antMatchers(Navigation.ACTUATOR_ALL_PATTERN_EXT).permitAll()
						.antMatchers(HttpMethod.POST, Navigation.AUTHENTICATE).permitAll()
						.anyRequest()
							.authenticated()
				.and()
					.addFilterBefore(new TokenAuthenticationFilter(tokenComponent, userDetailsService, tokenHolder), UsernamePasswordAuthenticationFilter.class);

	}
}
