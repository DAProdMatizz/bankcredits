package com.bank.credits.web.configuration.security;

import com.bank.credits.common.utils.StringUtils;
import io.jsonwebtoken.JwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

	private final TokenComponent tokenComponent;
	private final UserDetailsService userDetailsService;
	private final TokenHolder tokenHolder;

	TokenAuthenticationFilter(TokenComponent tokenComponent, UserDetailsService userDetailsService, TokenHolder tokenHolder) {

		this.tokenComponent = tokenComponent;
		this.userDetailsService = userDetailsService;
		this.tokenHolder = tokenHolder;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

		String token = tokenComponent.getToken(httpServletRequest);

		if (StringUtils.nonNullNonEmpty(token)) {

			try {
				String username = tokenComponent.getUsername(token);
				String identity = tokenComponent.getIdentity(httpServletRequest);

				UserDetails userDetails = userDetailsService.loadUserByUsername(username);
				TokenAuthentication tokenAuthentication = new TokenAuthentication(userDetails, token);
				SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);

				if (tokenComponent.isExpirationNear(token)) {
					String newToken = tokenComponent.generateToken(username);

					addToTokenHolder(identity, newToken, username);
					tokenComponent.applyToken(newToken, httpServletResponse);
				} else {
					addToTokenHolder(identity, token, username);
					tokenComponent.applyToken(token, httpServletResponse);
				}
			} catch (JwtException e) {
				LOGGER.debug("Invalid token", e);
			}
		}

		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

	// used for work with GUI
	// Should ignored for REST API
	private void addToTokenHolder(String identity, String token, String username) {

		if (StringUtils.nonNullNonEmpty(identity)) {
			tokenHolder.put(identity, new UserTokenPair(token, username));
		}
	}
}
