package com.bank.credits.web.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TokenHolder {

	private final Map<String, UserTokenPair> holder;

	@Autowired
	public TokenHolder() {

		holder = new ConcurrentHashMap<>();
	}

	public void put(String identity, UserTokenPair pair) {

		holder.put(identity, pair);
	}

	public UserTokenPair get(String identity) {

		return holder.get(identity);
	}

	public void remove(String identity) {

		holder.remove(identity);
	}

	public Map<String, UserTokenPair> getAll() {

		return holder;
	}
}
