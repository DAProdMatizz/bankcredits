package com.bank.credits.web.configuration.security;

import com.bank.credits.dto.user.UserDto;
import com.bank.credits.service.exception.UserNotFoundException;
import com.bank.credits.service.user.UserService;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserService userService;

	public UserDetailsServiceImpl(UserService userService) {

		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		try {
			UserDto user = userService.getUserByUsername(username);
			return new BankUserDetails(user);
		} catch (UserNotFoundException e) {
			throw new UsernameNotFoundException("Username not found", e);
		}
	}
}
