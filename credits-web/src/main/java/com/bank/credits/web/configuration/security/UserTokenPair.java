package com.bank.credits.web.configuration.security;

import java.util.Objects;

public class UserTokenPair {

	private final String token;
	private final String username;

	public UserTokenPair(String token, String username) {

		this.token = token;
		this.username = username;
	}

	public String getToken() {

		return token;
	}

	public String getUsername() {

		return username;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof UserTokenPair))
			return false;
		UserTokenPair that = (UserTokenPair) o;
		return Objects.equals(token, that.token) && Objects.equals(username, that.username);
	}

	@Override
	public int hashCode() {

		return Objects.hash(token, username);
	}
}
