package com.bank.credits.web.controller.authentication;

import com.bank.credits.dto.authenticate.AuthenticationDto;
import com.bank.credits.dto.authenticate.HandshakeDto;
import com.bank.credits.dto.user.UserDto;
import com.bank.credits.service.user.UserService;
import com.bank.credits.web.Navigation;
import com.bank.credits.web.configuration.security.TokenAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class AuthenticationController {

	private final AuthenticationManager authenticationManager;
	private final UserService userService;

	@Autowired
	public AuthenticationController(AuthenticationManager authenticationManager, UserService userService) {

		this.authenticationManager = authenticationManager;
		this.userService = userService;
	}

	@PostMapping(Navigation.AUTHENTICATE)
	public ResponseEntity<?> authenticate(@RequestBody AuthenticationDto authenticationDto) {

		String username = authenticationDto.getUsername();
		String password = authenticationDto.getPassword();

		TokenAuthentication authentication = (TokenAuthentication) authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

		UserDto user = userService.getUserByUsername(username);

		return ResponseEntity.ok(new HandshakeDto(authentication.getToken(), user.getUsername(), user.getRole(), UUID.randomUUID().toString()));
	}
}
