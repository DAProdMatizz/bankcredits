package com.bank.credits.web.controller.client;

import com.bank.credits.dto.client.ClientDto;
import com.bank.credits.dto.common.StringWrapper;
import com.bank.credits.service.client.ClientService;
import com.bank.credits.web.Navigation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Navigation.CLIENTS)
public class ClientController {

	private final ClientService clientService;

	@Autowired
	public ClientController(ClientService clientService) {

		this.clientService = clientService;
	}

	@PostMapping(Navigation.LOAD_ALL)
	public ResponseEntity<?> loadAll() {

		var clients = clientService.loadAll();
		return ResponseEntity.ok(clients);
	}

	@PostMapping(Navigation.LOAD_BY_ID)
	public ResponseEntity<?> loadById(@PathVariable(Navigation.ID) Long id) {

		ClientDto client = clientService.loadById(id);
		return ResponseEntity.ok(client);
	}

	@PostMapping(Navigation.LOAD)
	public ResponseEntity<?> loadByUsername(@RequestBody StringWrapper wrapper) {

		ClientDto client = clientService.loadByEmail(wrapper.getValue());
		return ResponseEntity.ok(client);
	}

	@PostMapping(Navigation.ADD)
	public ResponseEntity<?> add(@RequestBody ClientDto client) {

		long id = clientService.add(client);
		return ResponseEntity.ok(id);
	}

	@PostMapping(Navigation.UPDATE)
	public ResponseEntity<?> update(@RequestBody ClientDto client) {

		clientService.update(client);
		return ResponseEntity.ok().build();
	}

	@PostMapping(Navigation.DELETE_BY_ID)
	public ResponseEntity<?> delete(@PathVariable(Navigation.ID) Long id) {

		clientService.delete(id);
		return ResponseEntity.ok().build();
	}
}
