package com.bank.credits.web.controller.credit;

import com.bank.credits.dto.credit.CreditDto;
import com.bank.credits.dto.credit.CreditRequestDto;
import com.bank.credits.service.credit.CreditService;
import com.bank.credits.web.Navigation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Navigation.CREDITS)
public class CreditController {

	private final CreditService creditService;

	@Autowired
	public CreditController(CreditService creditService) {

		this.creditService = creditService;
	}

	@PostMapping(Navigation.LOAD_ALL)
	public ResponseEntity<?> loadAll() {

		var credits = creditService.loadAll();
		return ResponseEntity.ok(credits);
	}

	@PostMapping(Navigation.LOAD_BY_ID)
	public ResponseEntity<?> loadById(@PathVariable(Navigation.ID) Long id) {

		CreditDto credit = creditService.loadById(id);
		return ResponseEntity.ok(credit);
	}

	@PostMapping(Navigation.LOAD_BY_CLIENT_ID)
	public ResponseEntity<?> loadClientCredits(@PathVariable(Navigation.ID) Long clientId) {

		var credits = creditService.loadClientCredits(clientId);
		return ResponseEntity.ok(credits);
	}

	@PostMapping(Navigation.ADD)
	public ResponseEntity<?> createCredit(@RequestBody CreditRequestDto creditRequest) {

		long creditId = creditService.createCredit(creditRequest);
		return ResponseEntity.ok(creditId);
	}
}
