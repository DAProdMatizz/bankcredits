package com.bank.credits.web.controller.credit;

import com.bank.credits.dto.credit.CreditTypeDto;
import com.bank.credits.service.credit.CreditTypeService;
import com.bank.credits.web.Navigation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Navigation.CREDIT_TYPES)
public class CreditTypeController {

	private final CreditTypeService creditTypeService;

	@Autowired
	public CreditTypeController(CreditTypeService creditTypeService) {

		this.creditTypeService = creditTypeService;
	}

	@PostMapping(Navigation.LOAD_ALL)
	public ResponseEntity<?> loadAll() {

		var creditTypes = creditTypeService.loadAll();
		return ResponseEntity.ok(creditTypes);
	}

	@PostMapping(Navigation.LOAD_BY_ID)
	public ResponseEntity<?> loadById(@PathVariable(Navigation.ID) Long id) {

		CreditTypeDto creditType = creditTypeService.loadById(id);
		return ResponseEntity.ok(creditType);
	}

	@PostMapping(Navigation.ADD)
	public ResponseEntity<?> add(@RequestBody CreditTypeDto creditType) {

		long id = creditTypeService.add(creditType);
		return ResponseEntity.ok(id);
	}

	@PostMapping(Navigation.UPDATE)
	public ResponseEntity<?> update(@RequestBody CreditTypeDto creditType) {

		creditTypeService.update(creditType);
		return ResponseEntity.ok().build();
	}

	@PostMapping(Navigation.DELETE_BY_ID)
	public ResponseEntity<?> delete(@PathVariable(Navigation.ID) Long id) {

		creditTypeService.delete(id);
		return ResponseEntity.ok().build();
	}
}
