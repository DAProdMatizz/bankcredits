package com.bank.credits.web.controller.creditor;

import com.bank.credits.dto.creditor.CreditorDto;
import com.bank.credits.service.creditor.CreditorService;
import com.bank.credits.web.Navigation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Navigation.CREDITORS)
public class CreditorController {

	private final CreditorService creditorService;

	@Autowired
	public CreditorController(CreditorService creditorService) {

		this.creditorService = creditorService;
	}

	@PostMapping(Navigation.LOAD_ALL)
	public ResponseEntity<?> loadAll() {

		var creditors = creditorService.loadAll();
		return ResponseEntity.ok(creditors);
	}

	@PostMapping(Navigation.LOAD_BY_ID)
	public ResponseEntity<?> loadById(@PathVariable(Navigation.ID) Long id) {

		CreditorDto creditor = creditorService.loadById(id);
		return ResponseEntity.ok(creditor);
	}

	@PostMapping(Navigation.ADD)
	public ResponseEntity<?> add(@RequestBody CreditorDto creditor) {

		long id = creditorService.add(creditor);
		return ResponseEntity.ok(id);
	}

	@PostMapping(Navigation.UPDATE)
	public ResponseEntity<?> update(@RequestBody CreditorDto creditor) {

		creditorService.update(creditor);
		return ResponseEntity.ok().build();
	}

	@PostMapping(Navigation.DELETE_BY_ID)
	public ResponseEntity<?> deleteCreditor(@PathVariable(Navigation.ID) Long id) {

		creditorService.delete(id);
		return ResponseEntity.ok().build();
	}
}
