package com.bank.credits.web.controller.repayment;

import com.bank.credits.dto.repayment.RepaymentRequestDto;
import com.bank.credits.service.repayment.RepaymentService;
import com.bank.credits.web.Navigation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Navigation.REPAYMENTS)
public class RepaymentController {

	private final RepaymentService repaymentService;

	@Autowired
	public RepaymentController(RepaymentService repaymentService) {

		this.repaymentService = repaymentService;
	}

	@PostMapping(Navigation.LOAD_ALL)
	public ResponseEntity<?> loadAll() {

		var repayments = repaymentService.loadAll();
		return ResponseEntity.ok(repayments);
	}

	@PostMapping(Navigation.ADD)
	public ResponseEntity<?> addPayment(@RequestBody RepaymentRequestDto request) {

		long paymentId = repaymentService.addPayment(request);
		return ResponseEntity.ok(paymentId);
	}
}
